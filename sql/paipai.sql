/*
 Navicat Premium Data Transfer

 Source Server         : 私人阿里云
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 118.190.135.128:3306
 Source Schema         : paipai

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 22/07/2020 10:47:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_dept
-- ----------------------------
DROP TABLE IF EXISTS `base_dept`;
CREATE TABLE `base_dept`  (
  `dept_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'ID',
  `dept_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '部门编码',
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '部门名称',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '上级部门ID',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '部门状态 0激活 1锁定',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记 0:未删除  1:已删除',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_dept
-- ----------------------------
INSERT INTO `base_dept` VALUES ('1', '001', 'ΠΠ有限公司', '0', '0', 1, '0', '1', '1', '系统管理员', '2019-11-04 15:12:24', '1', '系统管理员', '2019-11-04 15:12:45');
INSERT INTO `base_dept` VALUES ('2', 'BJ001', '北京分公司', '1', '0', 1, '0', '1', '1', '系统管理员', '2019-11-07 18:21:23', '1', '系统管理员', '2019-11-07 18:21:23');
INSERT INTO `base_dept` VALUES ('5a58d349a9247b641dc57cfd68837658', 'BJ001CW', '法务部', '2', '0', 1, '0', NULL, '1', '系统管理员', '2019-11-07 18:29:04', '1', '系统管理员', '2019-11-07 18:33:04');
INSERT INTO `base_dept` VALUES ('7d3e3f4cfcb48f80958ea1e2c16b0639', 'NJ001', '南京分公司', '1', '0', 2, '0', NULL, '1', '系统管理员', '2019-11-07 18:21:29', '1', '系统管理员', '2019-11-07 18:21:29');

-- ----------------------------
-- Table structure for base_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `base_dict_type`;
CREATE TABLE `base_dict_type`  (
  `dict_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'ID',
  `dict_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '编码',
  `dict_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '字典名称',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记 0:未删除  1:已删除',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '字典主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_dict_type
-- ----------------------------
INSERT INTO `base_dict_type` VALUES ('4400a06d36fca95fb442dfbc5ee47b3e', 'sys_user_status', '系统用户状态', '0', '系统用户状态', '1', '系统管理员', '2019-11-07 17:52:13', '1', '系统管理员', '2019-11-07 17:52:13');
INSERT INTO `base_dict_type` VALUES ('7e7c0acabe2e14428167ec7a2170f21e', 'sys_dept_status', '系统部门状态', '0', '系统部门状态', '1', '系统管理员', '2019-11-07 17:52:27', '1', '系统管理员', '2019-11-07 17:52:27');
INSERT INTO `base_dict_type` VALUES ('b02ae8f01740d1f0332d66bf90172c1a', 'sys_log_type', '日志类型', '0', '日志类型', '1', '系统管理员', '2019-11-12 15:56:56', '1', '系统管理员', '2019-11-12 15:56:56');
INSERT INTO `base_dict_type` VALUES ('b6b482b9bee677cbc89cd3cd8ad06d3e', 'sys_data_scope', '系统数据权限', '0', '系统数据权限', '1', '系统管理员', '2019-11-12 16:38:53', '1', '系统管理员', '2019-11-12 16:38:53');
INSERT INTO `base_dict_type` VALUES ('fde6700630292e764cef7e0345e1bb8f', 'sys_menu_type', '系统菜单类型', '0', '系统菜单类型', '1', '系统管理员', '2019-11-08 10:15:29', '1', '系统管理员', '2019-11-08 10:15:29');

-- ----------------------------
-- Table structure for base_dict_value
-- ----------------------------
DROP TABLE IF EXISTS `base_dict_value`;
CREATE TABLE `base_dict_value`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '字典标签',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '字典值',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `dict_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '字典主表编码',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记 0：未删除   1：已删除',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '字典值表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_dict_value
-- ----------------------------
INSERT INTO `base_dict_value` VALUES ('007fe60ab1253a8b222bb9eae771aee7', '成功', 'S', 1, 'sys_log_type', '0', NULL, '1', '系统管理员', '2019-11-12 15:57:50', '1', '系统管理员', '2019-11-12 15:57:50');
INSERT INTO `base_dict_value` VALUES ('03bd874951bf5730361620b246457fe6', '按钮', 'F', 3, 'sys_menu_type', '0', NULL, '1', '系统管理员', '2019-11-08 10:16:09', '1', '系统管理员', '2019-11-08 10:16:09');
INSERT INTO `base_dict_value` VALUES ('08d4e1b99415423db138f66a254092a7', '全部数据权限', '1', 1, 'sys_data_scope', '0', NULL, '1', '系统管理员', '2019-11-12 16:39:13', '1', '系统管理员', '2019-11-12 16:39:13');
INSERT INTO `base_dict_value` VALUES ('1e8091d10ad4c32a3bae83971339ccca', '自定义数据权限', '5', 5, 'sys_data_scope', '0', NULL, '1', '系统管理员', '2019-11-12 17:43:13', '1', '系统管理员', '2019-11-12 17:43:13');
INSERT INTO `base_dict_value` VALUES ('2345d04cf0f5500a30bcd7eb64950cb6', '目录', 'M', 1, 'sys_menu_type', '0', NULL, '1', '系统管理员', '2019-11-08 10:15:50', '1', '系统管理员', '2019-11-08 10:15:50');
INSERT INTO `base_dict_value` VALUES ('32b41d60dc73038726fb0e193963bbe2', '停用', '1', 2, 'sys_dept_status', '0', NULL, '1', '系统管理员', '2019-11-07 17:55:09', '1', '系统管理员', '2019-11-07 17:55:09');
INSERT INTO `base_dict_value` VALUES ('4484d4971bb463b571a03a98ad5a7b75', '激活', '0', 1, 'sys_user_status', '0', NULL, '1', '系统管理员', '2019-11-07 17:33:12', '1', '系统管理员', '2019-11-07 17:33:12');
INSERT INTO `base_dict_value` VALUES ('7e7462b3e8437ad5b561ae33d0291055', '仅本人数据权限', '4', 4, 'sys_data_scope', '0', NULL, '1', '系统管理员', '2019-11-12 17:43:18', '1', '系统管理员', '2019-11-12 17:43:18');
INSERT INTO `base_dict_value` VALUES ('8f6bca3c445b27ff8682d746568717c5', '本部门数据权限', '2', 2, 'sys_data_scope', '0', NULL, '1', '系统管理员', '2019-11-12 16:58:10', '1', '系统管理员', '2019-11-12 16:58:10');
INSERT INTO `base_dict_value` VALUES ('d107f6fe9f372596a778b93ff970447c', '菜单', 'C', 2, 'sys_menu_type', '0', NULL, '1', '系统管理员', '2019-11-08 10:15:59', '1', '系统管理员', '2019-11-08 10:15:59');
INSERT INTO `base_dict_value` VALUES ('dce363786ab091853670319db76bf2ab', '失败', 'F', 2, 'sys_log_type', '0', NULL, '1', '系统管理员', '2019-11-12 15:57:59', '1', '系统管理员', '2019-11-12 15:57:59');
INSERT INTO `base_dict_value` VALUES ('e26e892bd183caa3b6e4e5252d0c309d', '自定义SQL', '6', 6, 'sys_data_scope', '0', NULL, '1', '系统管理员', '2019-11-13 10:05:40', '1', '系统管理员', '2019-11-13 10:05:40');
INSERT INTO `base_dict_value` VALUES ('e3bf3a9b9a122d3dd0bca33a7b8f8e45', '正常', '0', 1, 'sys_dept_status', '0', NULL, '1', '系统管理员', '2019-11-07 17:54:51', '1', '系统管理员', '2019-11-07 17:54:51');
INSERT INTO `base_dict_value` VALUES ('f9a4e0c8bd93b6189f47594d1b5d622c', '本部门及以下数据权限', '3', 3, 'sys_data_scope', '0', NULL, '1', '系统管理员', '2019-11-12 16:58:13', '1', '系统管理员', '2019-11-12 16:58:13');
INSERT INTO `base_dict_value` VALUES ('fb3ee5182131fea59066dc25aa2be871', '锁定', '1', 2, 'sys_user_status', '0', NULL, '1', '系统管理员', '2019-11-07 17:33:25', '1', '系统管理员', '2019-11-07 17:33:25');

-- ----------------------------
-- Table structure for base_gen_table
-- ----------------------------
DROP TABLE IF EXISTS `base_gen_table`;
CREATE TABLE `base_gen_table`  (
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '表名',
  `table_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '表描述',
  `class_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '类名',
  `temp_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '模板类型',
  `package_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '作者',
  `options` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记 0:未删除  1:已删除',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_log
-- ----------------------------
DROP TABLE IF EXISTS `base_log`;
CREATE TABLE `base_log`  (
  `log_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'ID',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '操作类型',
  `request_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求ID',
  `request_param` longtext CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '请求参数',
  `request_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求方法',
  `response_time` bigint(0) NULL DEFAULT NULL COMMENT '相应时间',
  `response_data` longtext CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '返回数据',
  `log_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '日志类型',
  `error_detail` longtext CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '错误信息',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT ' 删除标记 0：未删除  1：已删除',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_menu`;
CREATE TABLE `base_menu`  (
  `menu_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'ID',
  `menu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '菜单名称',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '上级目录ID',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '路径地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '组件',
  `is_frame` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否为外连(0否1是)',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '菜单类型',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否可见(0否1是)',
  `keep_alive` bit(1) NULL DEFAULT NULL COMMENT '是否缓存',
  `permission` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '显示图标',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记 0未删除 1已删除',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_menu
-- ----------------------------
INSERT INTO `base_menu` VALUES ('1', '系统管理', '0', 1, 'system', NULL, '1', 'M', '0', NULL, NULL, 'system', '0', '系统管理目录', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('100', '用户管理', '1', 1, 'user', 'system/user/index', '1', 'C', '0', NULL, 'system:user:list', 'user', '0', '用户管理菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1001', '用户查询', '100', 1, NULL, NULL, '1', 'F', '0', NULL, 'system:user:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1002', '用户新增', '100', 2, NULL, NULL, '1', 'F', '0', b'0', 'system:user:add', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1003', '用户修改', '100', 3, NULL, NULL, '1', 'F', '0', b'0', 'system:user:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1004', '用户删除', '100', 4, NULL, NULL, '1', 'F', '0', b'0', 'system:user:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1005', '用户导出', '100', 5, NULL, NULL, '1', 'F', '0', NULL, 'system:user:export', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1006', '用户导入', '100', 6, NULL, NULL, '1', 'F', '0', NULL, 'system:user:import', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1007', '重置密码', '100', 7, NULL, NULL, '1', 'F', '0', b'0', 'system:user:reset', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:43:44');
INSERT INTO `base_menu` VALUES ('1008', '角色查询', '101', 1, NULL, NULL, '1', 'F', '0', NULL, 'system:role:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1009', '角色新增', '101', 2, NULL, NULL, '1', 'F', '0', b'0', 'system:role:add', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('101', '角色管理', '1', 2, 'role', 'system/role/index', '1', 'C', '0', NULL, 'system:role:list', 'peoples', '0', '角色管理菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1010', '角色修改', '101', 3, NULL, NULL, '1', 'F', '0', b'0', 'system:role:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1011', '角色删除', '101', 4, NULL, NULL, '1', 'F', '0', b'0', 'system:role:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1012', '角色导出', '101', 5, NULL, NULL, '1', 'F', '0', NULL, 'system:role:export', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1013', '菜单查询', '102', 1, NULL, NULL, '1', 'F', '0', NULL, 'system:menu:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1014', '菜单新增', '102', 2, NULL, NULL, '1', 'F', '0', b'0', 'system:menu:add', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1015', '菜单修改', '102', 3, NULL, NULL, '1', 'F', '0', b'0', 'system:menu:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1016', '菜单删除', '102', 4, NULL, NULL, '1', 'F', '0', b'0', 'system:menu:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1017', '部门查询', '103', 1, NULL, NULL, '1', 'F', '0', NULL, 'system:dept:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1018', '部门新增', '103', 2, NULL, NULL, '1', 'F', '0', b'0', 'system:dept:add', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1019', '部门修改', '103', 3, NULL, NULL, '1', 'F', '0', b'0', 'system:dept:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('102', '菜单管理', '1', 3, 'menu', 'system/menu/index', '1', 'C', '0', NULL, 'system:menu:list', 'tree-table', '0', '菜单管理菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1020', '部门删除', '103', 4, NULL, NULL, '1', 'F', '0', b'0', 'system:dept:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1021', '岗位查询', '104', 1, NULL, NULL, '1', 'F', '0', NULL, 'system:post:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1022', '岗位新增', '104', 2, NULL, NULL, '1', 'F', '0', b'0', 'system:post:add', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1023', '岗位修改', '104', 3, NULL, NULL, '1', 'F', '0', b'0', 'system:post:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1024', '岗位删除', '104', 4, NULL, NULL, '1', 'F', '0', b'0', 'system:post:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1025', '岗位导出', '104', 5, NULL, NULL, '1', 'F', '0', NULL, 'system:post:export', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1026', '字典查询', '105', 1, '#', NULL, '1', 'F', '0', NULL, 'system:dict:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1027', '字典新增', '105', 2, '#', NULL, '1', 'F', '0', b'0', 'system:dict:add', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1028', '字典修改', '105', 3, '#', NULL, '1', 'F', '0', b'0', 'system:dict:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1029', '字典删除', '105', 4, '#', NULL, '1', 'F', '0', b'0', 'system:dict:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('103', '部门管理', '1', 4, 'dept', 'system/dept/index', '1', 'C', '0', NULL, 'system:dept:list', 'tree', '0', '部门管理菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1030', '字典导出', '105', 5, '#', NULL, '1', 'F', '0', NULL, 'system:dict:export', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1036', '公告查询', '107', 1, '#', NULL, '1', 'F', '0', NULL, 'system:notice:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', NULL, '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1037', '公告新增', '107', 2, '#', NULL, '1', 'F', '0', b'0', 'system:notice:add', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1038', '公告修改', '107', 3, '#', NULL, '1', 'F', '0', b'0', 'system:notice:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1039', '公告删除', '107', 4, '#', NULL, '1', 'F', '0', b'0', 'system:notice:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('104', '岗位管理', '1', 5, 'post', 'system/post/index', '1', 'C', '0', NULL, 'system:post:list', 'post', '0', '岗位管理菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1046', '在线查询', '109', 1, '#', NULL, '1', 'F', '0', NULL, 'monitor:online:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1047', '批量强退', '109', 2, '#', NULL, '1', 'F', '0', NULL, 'monitor:online:batchLogout', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1048', '单条强退', '109', 3, '#', NULL, '1', 'F', '0', NULL, 'monitor:online:forceLogout', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1049', '任务查询', '110', 1, '#', NULL, '1', 'F', '0', NULL, 'monitor:job:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('105', '字典管理', '1', 6, 'dict', 'system/dict/index', '1', 'C', '0', NULL, 'system:dict:list', 'dict', '0', '字典管理菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1050', '任务新增', '110', 2, '#', NULL, '1', 'F', '0', NULL, 'monitor:job:add', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1051', '任务修改', '110', 3, '#', NULL, '1', 'F', '0', NULL, 'monitor:job:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1052', '任务删除', '110', 4, '#', NULL, '1', 'F', '0', NULL, 'monitor:job:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1053', '状态修改', '110', 5, '#', NULL, '1', 'F', '0', NULL, 'monitor:job:changeStatus', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1054', '任务导出', '110', 7, '#', NULL, '1', 'F', '0', NULL, 'monitor:job:export', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1055', '生成查询', '114', 1, '#', NULL, '1', 'F', '0', NULL, 'tool:gen:query', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1056', '生成修改', '114', 2, '#', NULL, '1', 'F', '0', NULL, 'tool:gen:edit', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1057', '生成删除', '114', 3, '#', NULL, '1', 'F', '0', NULL, 'tool:gen:remove', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1058', '预览代码', '114', 4, '#', NULL, '1', 'F', '0', NULL, 'tool:gen:preview', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('1059', '生成代码', '114', 5, '#', NULL, '1', 'F', '0', NULL, 'tool:gen:code', '#', '0', NULL, '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('107', '通知公告', '1', 8, 'notice', 'system/notice/index', '1', 'C', '1', b'0', 'system:notice:list', 'message', '0', '通知公告菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-13 15:10:49');
INSERT INTO `base_menu` VALUES ('109', '在线用户', '2', 1, 'online', 'monitor/online/index', '1', 'C', '0', NULL, 'monitor:online:list', 'online', '0', '在线用户菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('110', '定时任务', '2', 2, 'job', 'monitor/job/index', '1', 'C', '0', NULL, 'monitor:job:list', 'job', '0', '定时任务菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('111', '数据监控', '2', 3, 'druid', 'monitor/druid/index', '1', 'C', '0', NULL, 'monitor:druid:list', 'druid', '0', '数据监控菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('112', '服务监控', '2', 4, 'server', 'monitor/server/index', '1', 'C', '0', NULL, 'monitor:server:list', 'server', '0', '服务监控菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('114', '代码生成', '3', 2, 'server', 'tools/generator/index', '1', 'C', '0', b'0', 'tool:gen:list', 'code', '0', '代码生成菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2020-01-21 11:22:04');
INSERT INTO `base_menu` VALUES ('115', '系统接口', '3', 3, 'swagger', 'tools/swagger/index', '1', 'C', '0', b'0', 'tool:swagger:list', 'swagger', '0', '系统接口菜单', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2020-01-21 11:22:17');
INSERT INTO `base_menu` VALUES ('2', '系统监控', '0', 2, 'monitor', NULL, '1', 'M', '0', NULL, NULL, 'monitor', '0', '系统监控目录', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2019-11-08 17:42:50');
INSERT INTO `base_menu` VALUES ('3', '系统工具', '0', 3, 'tools', NULL, '1', 'M', '0', b'0', NULL, 'tool', '0', '系统工具目录', '1', '系统管理员', '2019-11-08 17:42:50', '1', '系统管理员', '2020-01-21 11:20:47');
INSERT INTO `base_menu` VALUES ('31fe964bd5d169dd3f7acba9a086ed26', '系统缓存', '2', 6, 'redis', 'monitor/redis/index', '1', 'C', '0', b'1', NULL, 'nested', '0', NULL, '1', '系统管理员', '2019-11-12 10:38:31', '1', '系统管理员', '2019-11-12 10:40:48');
INSERT INTO `base_menu` VALUES ('da5329b071b22d0892a650f44e7095d2', '操作日志', '2', 5, 'log', 'monitor/log/index', '1', 'C', '0', b'1', NULL, 'log', '0', NULL, '1', '系统管理员', '2019-11-12 10:36:39', '1', '系统管理员', '2019-11-12 10:41:05');

-- ----------------------------
-- Table structure for base_post
-- ----------------------------
DROP TABLE IF EXISTS `base_post`;
CREATE TABLE `base_post`  (
  `post_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'ID',
  `post_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '职位编码',
  `post_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '职位名称',
  `dept_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '所属部门ID',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '职位状态',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '职位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_post
-- ----------------------------
INSERT INTO `base_post` VALUES ('80e46d73fa77cba21d056962c05a4b9a', '002', 'CPO', NULL, '0', 2, '0', NULL, '1', '系统管理员', '2019-11-11 18:16:46', '1', '系统管理员', '2019-11-11 18:28:45');
INSERT INTO `base_post` VALUES ('f78535ba0b22cf3219f48da75954ff68', '001', 'CEO', NULL, '0', 1, '0', NULL, '1', '系统管理员', '2019-11-11 18:13:06', '1', '系统管理员', '2019-11-11 18:13:06');

-- ----------------------------
-- Table structure for base_quartz_job
-- ----------------------------
DROP TABLE IF EXISTS `base_quartz_job`;
CREATE TABLE `base_quartz_job`  (
  `job_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'ID',
  `bean_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'Spring Bean名称',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'cron 表达式',
  `is_pause` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '状态：1暂停、0启用',
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'Job名称',
  `method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '方法名',
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '参数',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `del_flag` char(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`job_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统定时任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_quartz_job
-- ----------------------------
INSERT INTO `base_quartz_job` VALUES ('735b7afaab7e30123d4e14ecaa82d4f6', 'visitsTask', '0 0 0 * * ?', '0', '更新访客记录', 'run', NULL, '每日0点创建新的访客记录', '0', '1', '系统管理员', '2019-11-13 14:56:02', '1', '系统管理员', '2019-11-13 14:56:02');

-- ----------------------------
-- Table structure for base_quartz_log
-- ----------------------------
DROP TABLE IF EXISTS `base_quartz_log`;
CREATE TABLE `base_quartz_log`  (
  `log_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bean_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_success` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `time` bigint(0) NULL DEFAULT NULL,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `exception_detail` longtext CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `crt_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '定时任务日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role`  (
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'ID',
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色编码',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色名称',
  `sort` int(0) NULL DEFAULT NULL COMMENT '显示序列',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '数据权限',
  `scope_sql` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '自定义sql',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '状态(0有效 1失效）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记(0未删除 1已删除)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_role
-- ----------------------------
INSERT INTO `base_role` VALUES ('1', 'admin', '超级管理员', 1, '1', NULL, '0', '0', NULL, '1', '1系统管理员', '2019-11-04 15:15:07', '1', '系统管理员', '2019-11-04 15:15:16');
INSERT INTO `base_role` VALUES ('5cbd33ac70531e016540ae4748babe03', 'common', '普通角色', 2, '2', NULL, '0', '0', NULL, '1', '系统管理员', '2019-11-08 18:43:38', '1', '系统管理员', '2019-11-13 11:41:30');
INSERT INTO `base_role` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', 'common2', '普通角色2', 2, '6', ' or t.dept_id in (\'5a58d349a9247b641dc57cfd68837658\',\'7d3e3f4cfcb48f80958ea1e2c16b0639\')', '0', '0', NULL, '1', '系统管理员', '2019-11-13 11:41:51', '1', '系统管理员', '2019-11-13 11:41:51');

-- ----------------------------
-- Table structure for base_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `base_role_dept`;
CREATE TABLE `base_role_dept`  (
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `dept_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色数据权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_role_menu`;
CREATE TABLE `base_role_menu`  (
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色ID',
  `menu_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单ID'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色-菜单关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_role_menu
-- ----------------------------
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '100');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1001');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1002');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1003');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1004');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1005');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1006');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1007');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '101');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1008');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1009');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1010');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1011');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1012');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '102');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1013');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1014');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1015');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1016');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '103');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1017');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1018');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1019');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1020');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '104');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1021');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1022');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1023');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1024');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1025');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '105');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1026');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1027');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1028');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1029');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1030');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '107');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1036');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1037');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1038');
INSERT INTO `base_role_menu` VALUES ('5cbd33ac70531e016540ae4748babe03', '1039');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '100');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1001');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1002');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1003');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1004');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1005');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1006');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1007');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '101');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1008');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1009');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1010');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1011');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1012');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '102');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1013');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1014');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1015');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1016');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '103');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1017');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1018');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1019');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1020');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '104');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1021');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1022');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1023');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1024');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1025');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '105');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1026');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1027');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1028');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1029');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1030');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '107');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1036');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1037');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1038');
INSERT INTO `base_role_menu` VALUES ('adb521ea2d1b35e8ff1a6e316732cd16', '1039');

-- ----------------------------
-- Table structure for base_serial_no
-- ----------------------------
DROP TABLE IF EXISTS `base_serial_no`;
CREATE TABLE `base_serial_no`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `module` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '功能模块',
  `prefix` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '前缀',
  `mode` int(0) NULL DEFAULT NULL COMMENT '年月日格式',
  `length` int(0) NULL DEFAULT NULL COMMENT '流水码长度',
  `num` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '当前号码累计',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '流水号生成器' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user`  (
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'id',
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登录名',
  `user_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '员工号',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '密码',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '头像地址',
  `mobile_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '移动电话',
  `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '固定电话',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮箱',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '用户状态 0激活 1锁定',
  `user_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户类型(user_type)',
  `dept_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '部门ID',
  `open_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '微信ID',
  `del_flag` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标记 0：未删除  1：已删除',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人名称',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人ID',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人名称',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES ('1', 'admin', '000000', '系统管理员', 'e10adc3949ba59abbe56e057f20f883e', 'https://i.loli.net/2019/04/04/5ca5b971e1548.jpeg', '13888888888', NULL, '123@qq.com', '0', '1', '1', NULL, '0', NULL, '1', '系统管理员', '2019-11-01 16:26:51', '1', '系统管理员', '2019-11-01 16:27:12');
INSERT INTO `base_user` VALUES ('a1425dbaaaa7b7510c3698cab276dfda', 'common', NULL, '普通用户', 'e10adc3949ba59abbe56e057f20f883e', 'https://i.loli.net/2018/12/06/5c08894d8de21.jpg', NULL, NULL, '1122@qq.com', '0', NULL, '7d3e3f4cfcb48f80958ea1e2c16b0639', NULL, '0', NULL, '1', '系统管理员', '2019-11-11 10:08:17', 'a1425dbaaaa7b7510c3698cab276dfda', '普通用户', '2019-11-11 10:37:18');
INSERT INTO `base_user` VALUES ('d96b42cebd550cd501b2da19b4aad3b0', 'test', NULL, '测试用户', 'e10adc3949ba59abbe56e057f20f883e', 'https://i.loli.net/2018/12/06/5c08894d8de21.jpg', NULL, NULL, 'a@qq.com', '0', NULL, '5a58d349a9247b641dc57cfd68837658', NULL, '0', NULL, '1', '系统管理员', '2019-11-11 10:38:31', '1', '系统管理员', '2019-11-13 11:42:23');

-- ----------------------------
-- Table structure for base_user_post
-- ----------------------------
DROP TABLE IF EXISTS `base_user_post`;
CREATE TABLE `base_user_post`  (
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户ID',
  `post_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '职位ID'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户-职位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_user_role`;
CREATE TABLE `base_user_role`  (
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户ID',
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色ID'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户-角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user_role
-- ----------------------------
INSERT INTO `base_user_role` VALUES ('1', '1');
INSERT INTO `base_user_role` VALUES ('a1425dbaaaa7b7510c3698cab276dfda', '5cbd33ac70531e016540ae4748babe03');
INSERT INTO `base_user_role` VALUES ('d96b42cebd550cd501b2da19b4aad3b0', 'adb521ea2d1b35e8ff1a6e316732cd16');

-- ----------------------------
-- Table structure for base_visit
-- ----------------------------
DROP TABLE IF EXISTS `base_visit`;
CREATE TABLE `base_visit`  (
  `visit_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'ID',
  `date` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '日期',
  `ip_counts` bigint(0) NULL DEFAULT NULL COMMENT 'IP统计',
  `pv_counts` bigint(0) NULL DEFAULT NULL COMMENT '用户统计',
  `week_day` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '工作日',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `crt_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `crt_time` datetime(0) NULL DEFAULT NULL,
  `upd_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `upd_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`visit_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户访问' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Function structure for FUNC_GET_DEPT_CHILD
-- ----------------------------
DROP FUNCTION IF EXISTS `FUNC_GET_DEPT_CHILD`;
delimiter ;;
CREATE FUNCTION `FUNC_GET_DEPT_CHILD`(`in_dept_id` varchar(100))
 RETURNS longtext CHARSET utf8 COLLATE utf8_bin
BEGIN
	DECLARE child_ids LONGTEXT DEFAULT '';
	DECLARE temp_id LONGTEXT DEFAULT '';
	SET temp_id = `in_dept_id`;
	WHILE temp_id IS NOT NULL DO
		IF child_ids = '' THEN
				SET child_ids = temp_id;
		ELSE
				SET child_ids = CONCAT(child_ids, ',', temp_id);
		END IF;
		SELECT
			GROUP_CONCAT(`dept_id`)INTO temp_id
		FROM
			base_dept
		WHERE
			find_in_set(parent_id, temp_id) > 0 ;
		END WHILE;
	RETURN child_ids;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for PROC_GENERATE_SERIAL_NO
-- ----------------------------
DROP PROCEDURE IF EXISTS `PROC_GENERATE_SERIAL_NO`;
delimiter ;;
CREATE PROCEDURE `PROC_GENERATE_SERIAL_NO`(IN `module` varchar(50),IN `prefix` varchar(50),
IN `mode` int,IN `length` int,OUT `newNo` varchar(50))
BEGIN
	DECLARE YMD VARCHAR(50) DEFAULT '';
	DECLARE CURRENT_NO VARCHAR(50) DEFAULT '';
	DECLARE ROWID INT;
	DECLARE ROWNUM INT;
	-- 获取年月日
	IF `mode` = 1 THEN
		SELECT DATE_FORMAT(NOW(),'%Y%m%d') INTO YMD;
	ELSEIF `mode` = 2 THEN
		SELECT DATE_FORMAT(NOW(),'%y%m%d') INTO YMD;
	ELSEIF `mode` = 3 THEN
		SELECT DATE_FORMAT(NOW(),'%Y%m')   INTO YMD;
	ELSEIF `mode` = 4 THEN
		SELECT DATE_FORMAT(NOW(),'%y%m')   INTO YMD;
	ELSEIF `mode` = 5 THEN
		SELECT DATE_FORMAT(NOW(),'%Y')     INTO YMD;
	ELSEIF `mode` = 6 THEN
		SELECT DATE_FORMAT(NOW(),'%y')     INTO YMD;
	ELSE
		SELECT DATE_FORMAT(NOW(),'%y')     INTO YMD;
	END IF;
	-- 查询相同条件是否生成过流水号
	SELECT COUNT(1) INTO ROWNUM FROM base_serial_no s WHERE s.module = module AND s.`mode` = `mode` AND s.length = length;
	IF ROWNUM = 0 THEN
		SET CURRENT_NO = LPAD('1',length,'0');
		INSERT INTO base_serial_no(module,prefix,mode,length,num) VALUES(module,prefix,mode,length,CURRENT_NO);
	ELSE
		SELECT id,num+1 INTO ROWID,CURRENT_NO FROM base_serial_no s WHERE s.module = module AND s.`mode` = `mode` AND s.length = length;
		SET CURRENT_NO = LPAD(CURRENT_NO,length,'0');
		UPDATE base_serial_no SET num = CURRENT_NO WHERE id = ROWID;
	END IF;
	IF prefix IS NULL OR prefix = '' THEN
		SET newNo = CONCAT(YMD,CURRENT_NO);
	ELSE
		SET newNo = CONCAT(prefix,YMD,CURRENT_NO);
	END IF;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
