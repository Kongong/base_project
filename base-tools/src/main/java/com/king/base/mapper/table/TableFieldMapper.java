package com.king.base.mapper.table;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.table.TableField;

public interface TableFieldMapper extends BaseMapper<TableField> {
}
