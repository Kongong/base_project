package com.king.base.mapper.table;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.table.TableData;

public interface TableDataMapper extends BaseMapper<TableData> {
}
