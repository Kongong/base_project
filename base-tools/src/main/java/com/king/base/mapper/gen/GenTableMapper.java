package com.king.base.mapper.gen;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.domain.gen.GenTable;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Spur
 * @date 2019-12-11
 */
public interface GenTableMapper extends BaseMapper<GenTable> {

    List<GenTable> page(Page<GenTable> page, @Param("bean")GenTable genTable);
}
