package com.king.base.mapper.gen;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.gen.GenTableColumn;

/**
 * @author Spur
 * @date 2019-12-11
 */
public interface GenTableColumnMapper extends BaseMapper<GenTableColumn> {
}
