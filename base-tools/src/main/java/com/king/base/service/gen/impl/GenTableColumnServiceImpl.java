package com.king.base.service.gen.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.gen.GenTableColumn;
import com.king.base.mapper.gen.GenTableColumnMapper;
import com.king.base.service.gen.GenTableColumnService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Spur
 * @date 2019-12-11
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GenTableColumnServiceImpl extends ServiceImpl<GenTableColumnMapper,GenTableColumn> implements GenTableColumnService {
}
