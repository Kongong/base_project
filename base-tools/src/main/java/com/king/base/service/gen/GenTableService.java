package com.king.base.service.gen;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.gen.GenTable;
import org.springframework.data.domain.Pageable;

/**
 * @author Spur
 * @date 2019-12-11
 */
public interface GenTableService extends IService<GenTable> {
    /**
     * 数据库表分页查询
     * @param page
     * @param bean
     * @return
     */
    IPage<GenTable> page(Pageable page, GenTable bean);
}
