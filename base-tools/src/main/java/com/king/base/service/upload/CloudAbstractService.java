package com.king.base.service.upload;

import cn.hutool.core.date.DateTime;
import com.king.base.config.cloud.CloudConfig;
import com.king.base.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

/**
 * @author ππ
 * @date 2020-4-23 15:13
 * 云存储抽象类
 */

public abstract class CloudAbstractService {
    protected static Logger logger = LoggerFactory.getLogger(CloudAbstractService.class);
    protected CloudConfig config;

    public CloudAbstractService(CloudConfig config) {
        this.config = config;
    }

    /**
     * 获取路径  文件名为UUID
     * @param suffix 后缀 .jpg
     * @return
     */
    public String getPathSuffix(String suffix){
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        String path = DateTime.now().toString("yyyyMMdd") + "/" +uuid;
        if(StringUtils.isNotBlank(config.getPrefix())){
            path = config.getPrefix() + "/" +path;
        }
        return path;
    }

    /**
     *获取文件路径
     * @param fileName 文件名全称   test.pdf
     * @return
     */
    public String getPath(String fileName){
        String path = DateTime.now().toString("yyyyMMdd") + "/" +fileName;
        if(StringUtils.isNotBlank(config.getPrefix())){
            path = config.getPrefix() + "/" +path;
        }
        return path;
    }

    /**
     * 文件上传
     * @param data 文件字节数组
     * @param path 文件上传路径
     * @return
     */
    public abstract String upload(byte[] data,String path);

    /**
     * 文件上传
     * @param data 文件字节数组
     * @param suffix 文件后缀
     * @return
     */
    public abstract String uploadSuffix(byte[] data,String suffix);

    /**
     * 文件上传
     * @param inputStream 字节流
     * @param path 路径 包含文件名
     * @return
     */
    public abstract String upload(InputStream inputStream,String path);

    /**
     * 文件上传
     * @param inputStream 字节流
     * @param suffix 文件后缀
     * @return
     */
    public abstract String uploadSuffix(InputStream inputStream,String suffix);

    /**
     * 文件上传
     * @param file 文件对象
     * @return
     */
    public abstract String upload(File file);

    /**
     * 通过url删除文件
     * @param url
     * @return
     */
    public abstract boolean delete(String url);

    /**
     * 查询指定文件夹下的文件
     * @param folder
     */
    public abstract void queryAll(String folder);
}
