package com.king.base.service.gen;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.gen.GenTableColumn;

/**
 * @author Spur
 * @date 2019-12-11
 */
public interface GenTableColumnService extends IService<GenTableColumn> {
}
