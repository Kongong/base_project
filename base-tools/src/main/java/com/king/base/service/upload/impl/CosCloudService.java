package com.king.base.service.upload.impl;
import com.king.base.config.cloud.CloudConfig;
import com.king.base.service.upload.CloudAbstractService;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;

import java.io.File;
import java.io.InputStream;

/**
 * @author ππ
 * @date 2020-4-23 15:41
 * 腾讯云存储
 */

public class CosCloudService extends CloudAbstractService {
    private COSClient client;
    public CosCloudService(CloudConfig config) {
        super(config);
        init();
    }

    private void init() {
        COSCredentials cred = new BasicCOSCredentials(config.getAccessKey(),config.getSecretKey());
        ClientConfig clientConfig = new ClientConfig(new Region(config.getRegion()));
        client = new COSClient(cred,clientConfig);
    }

    @Override
    public String upload(byte[] data, String path) {
        //腾讯云必需要以"/"开头
        if(!path.startsWith("/")) {
            path = "/" + path;
        }

        //上传到腾讯云
        return null;
    }

    @Override
    public String uploadSuffix(byte[] data, String suffix) {
        return null;
    }

    @Override
    public String upload(InputStream inputStream, String path) {
        return null;
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix) {
        return null;
    }

    @Override
    public String upload(File file) {

        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(config.getBucket(),file.getName(),file);
            PutObjectResult putObjectResult = client.putObject(putObjectRequest);
        }catch (Exception e){
            logger.error(e.getMessage());
        }finally {
            client.shutdown();
        }
        return config.getDomain() + "/" +file.getName();
    }

    @Override
    public boolean delete(String url) {
        return false;
    }

    @Override
    public void queryAll(String folder) {

    }
}
