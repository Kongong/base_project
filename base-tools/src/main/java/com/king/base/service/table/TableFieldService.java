package com.king.base.service.table;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.table.TableField;

public interface TableFieldService extends IService<TableField> {
}
