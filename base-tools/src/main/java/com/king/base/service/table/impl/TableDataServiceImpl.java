package com.king.base.service.table.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.table.TableData;
import com.king.base.mapper.table.TableDataMapper;
import com.king.base.service.table.TableDataService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class TableDataServiceImpl extends ServiceImpl<TableDataMapper, TableData> implements TableDataService {
}
