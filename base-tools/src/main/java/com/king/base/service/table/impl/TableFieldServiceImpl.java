package com.king.base.service.table.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.table.TableField;
import com.king.base.mapper.table.TableFieldMapper;
import com.king.base.service.table.TableFieldService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ππ
 * @date 2020-1-22 15:42
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TableFieldServiceImpl extends ServiceImpl<TableFieldMapper, TableField> implements TableFieldService {
}
