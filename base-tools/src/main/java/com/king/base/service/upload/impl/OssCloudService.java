package com.king.base.service.upload.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ListObjectsRequest;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.king.base.config.cloud.CloudConfig;
import com.king.base.service.upload.CloudAbstractService;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * @author ππ
 * @date 2020-4-23 15:40
 * 阿里云存储
 */

public class OssCloudService extends CloudAbstractService {
    private OSS client;
    public OssCloudService(CloudConfig config) {
        super(config);
        init();
    }

    private void init(){
        client = new OSSClientBuilder().build(config.getDomain(),config.getAccessKey(),config.getSecretKey());
    }

    @Override
    public String upload(byte[] data, String path) {
        return upload(new ByteArrayInputStream(data),path);
    }

    @Override
    public String uploadSuffix(byte[] data, String suffix) {
        return upload(data,getPathSuffix(suffix));
    }

    @Override
    public String upload(InputStream inputStream, String path) {
        try {
            client.putObject(config.getBucket(),path,inputStream);
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
        }finally {
            client.shutdown();
        }
        return config.getDomain() + "/" +path;
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix) {
        return upload(inputStream,getPathSuffix(suffix));
    }

    @Override
    public String upload(File file) {
        try {
            String path = getPath(file.getName());
            client.putObject(config.getBucket(),path,file);
            return config.getDomain() + "/" +path;
        }catch (Exception e){
            logger.error(e.getMessage());
        }finally {
            client.shutdown();
        }
        return null;
    }

    @Override
    public boolean delete(String url) {
        url = getFileNameByUrl(url);
        if(checkIsExist(url)){
            try {
                client.deleteObject(config.getBucket(),url);
            }catch (Exception e){
                client.shutdown();
            }
        }
        return false;
    }

    private String getFileNameByUrl(String url) {
        if(null != url && url.indexOf("/")>-1){
            url = url.substring(url.lastIndexOf("/")+1);
        }
        return url;
    }

    private boolean checkIsExist(String url) {
        return client.doesObjectExist(config.getBucket(),url);
    }

    @Override
    public void queryAll(String folder) {
        final int maxKeys = 200;
        String nextMarker = null;
        ObjectListing objectListing;
        try {
            do {
                objectListing = client.listObjects(new ListObjectsRequest(config.getBucket()).withMarker(nextMarker).withMaxKeys(maxKeys).withPrefix(folder));

                List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
                for (OSSObjectSummary s : sums) {
                    System.out.println("\t" + s.getKey());
                }

                nextMarker = objectListing.getNextMarker();

            } while (objectListing.isTruncated());
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
        }finally {
            client.shutdown();
        }
    }
}
