package com.king.base.service.gen.impl;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.gen.GenTable;
import com.king.base.mapper.gen.GenTableMapper;
import com.king.base.service.gen.GenTableService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * @author Spur
 * @date 2019-12-11
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GenTableServiceImpl extends ServiceImpl<GenTableMapper,GenTable> implements GenTableService {
    @Override
    public IPage<GenTable> page(Pageable pageable, GenTable bean) {
        Page<GenTable> page=new Page<>(pageable.getPageNumber(),pageable.getPageSize());
        List<GenTable> list=baseMapper.page(page,bean);
        return page.setRecords(list);
    }
}
