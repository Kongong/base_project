package com.king.base.service.upload.factory;

import com.king.base.config.cloud.CloudConfig;
import com.king.base.constant.CloudConstant;
import com.king.base.service.upload.CloudAbstractService;
import com.king.base.service.upload.impl.CosCloudService;
import com.king.base.service.upload.impl.OssCloudService;
import com.king.base.service.upload.impl.QiNiuCloudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ππ
 * @date 2020-4-23 15:52
 */
@Component
public class CloudStorageFactory {
    @Autowired
    private CloudConfig config;
    public CloudAbstractService builder(){
        if(CloudConstant.TYPE_ALIYUN.equals(config.getType())){
            return new OssCloudService(config);
        }else if(CloudConstant.TYPE_QCLOUD.equals(config.getType())){
            return new CosCloudService(config);
        }else  if(CloudConstant.TYPE_QINIU.equals(config.getType())){
            return new QiNiuCloudService(config);
        }
        return null;
    }
}
