package com.king.base.rest.upload;

import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.upload.factory.CloudStorageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ππ
 * @date 2020-4-27 11:26
 */
@RequestMapping(value = "/api/tools/upload")
@RestController
public class UploadController {
    @Autowired
    private CloudStorageFactory cloudStorageFactory;


    @RequestMapping(value = "/uploadFile")
    public BaseResponseEntity upload(@RequestParam("file") MultipartFile file) throws Exception{
        BaseResponseEntity baseResponseEntity = new BaseResponseEntity();
        if(file.isEmpty()){
            baseResponseEntity.setSuccess(false);
            baseResponseEntity.setMessage("上传文件不能为空!");
            return baseResponseEntity;
        }
        String url = cloudStorageFactory.builder().upload(file.getInputStream(),file.getName());
        baseResponseEntity.setData(url);
        return baseResponseEntity;
    }

    @GetMapping(value = "/query")
    public BaseResponseEntity query(){
        cloudStorageFactory.builder().queryAll("pic/");
        return new BaseResponseEntity();
    }

    @GetMapping(value = "/delete/{url}")
    public BaseResponseEntity delete(@PathVariable String url){
        cloudStorageFactory.builder().delete("https://lazhub2b.oss-cn-beijing.aliyuncs.com/logon.png");
        return new BaseResponseEntity();
    }
}
