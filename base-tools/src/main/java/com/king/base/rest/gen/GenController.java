package com.king.base.rest.gen;

import com.king.base.domain.gen.GenTable;
import com.king.base.service.gen.GenTableColumnService;
import com.king.base.service.gen.GenTableService;
import com.king.base.utils.PageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Spur
 * @date 2019-12-11
 */
@RequestMapping(value = "api/tool/gen")
@RestController
@Api(value = "gen",description = "代码生成器")
public class GenController {
    @Autowired
    private GenTableService genTableService;
    @Autowired
    private GenTableColumnService genTableColumnService;

    @GetMapping(value = "/page")
    @ApiOperation(value = "分页查询数据库表")
    public ResponseEntity page(Pageable pageable, GenTable genTable){
        return new ResponseEntity(PageUtil.toPage(genTableService.page(pageable,genTable)),HttpStatus.OK);
    }
}
