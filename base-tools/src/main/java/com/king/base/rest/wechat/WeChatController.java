package com.king.base.rest.wechat;
import com.king.base.config.weChat.WeChatClient;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Spur
 * @date 2019/8/20
 */
@RestController
@RequestMapping(value = "api/tools/wechat")
@Api(value = "wechat",description = "微信Controller")
public class WeChatController {
    @Autowired
    private WeChatClient weChatClient;

    @PostMapping(value = "/sign")
    public Map<String, String> sign(@RequestParam String url){
        return weChatClient.sign(url);
    }
}
