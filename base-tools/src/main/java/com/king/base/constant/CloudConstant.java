package com.king.base.constant;

/**
 * @author ππ
 * @date 2020-4-23 15:46
 */

public class CloudConstant {
    /**
     * 云存储 1：阿里云 2：腾讯云 3：七牛云
     */
    public static final Integer TYPE_ALIYUN = 1;
    public static final Integer TYPE_QCLOUD = 2;
    public static final Integer TYPE_QINIU = 3;

}
