package com.king.base.config.weChat;

import java.io.Serializable;

/**
 * 应用推送消息--文本消息的消息内容
 * @author Spur
 * @date 2019/8/20
 */
public class Text implements Serializable {

    private static final long serialVersionUID = -5154376040184013733L;

    /**
     * 消息内容，最长不超过2048个字节
     */
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}