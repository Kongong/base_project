package com.king.base.config.weChat;

import java.io.Serializable;

/**
 * @author Spur
 * @date 2019/8/20
 */
public class TextCardMsg extends BaseMsg implements Serializable {
    private static final long serialVersionUID = -4756476040184013733L;

    /**
     * 消息内容
     */
    private TextCard textCard;

    public TextCard getTextCard() {
        return textCard;
    }

    public void setTextCard(TextCard textCard) {
        this.textCard = textCard;
    }
}
