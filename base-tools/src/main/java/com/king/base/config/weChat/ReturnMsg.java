package com.king.base.config.weChat;

import java.io.Serializable;

/**
 *  应用推送消息--返回消息
 * @author Spur
 * @date 2019/8/20
 */
public class ReturnMsg implements Serializable {

    private static final long serialVersionUID = -5433236040184013733L;

    /**
     * 错误码
     */
    private int errcode;

    /**
     * 错误信息
     */
    private String errmsg;

    /**
     * 无效用户
     */
    private String invaliduser;

    /**
     * 无效部门
     */
    private String invalidparty;

    /**
     * 无效标签
     */
    private String invalidtag;

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getInvaliduser() {
        return invaliduser;
    }

    public void setInvaliduser(String invaliduser) {
        this.invaliduser = invaliduser;
    }

    public String getInvalidparty() {
        return invalidparty;
    }

    public void setInvalidparty(String invalidparty) {
        this.invalidparty = invalidparty;
    }

    public String getInvalidtag() {
        return invalidtag;
    }

    public void setInvalidtag(String invalidtag) {
        this.invalidtag = invalidtag;
    }
}

