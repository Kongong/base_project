package com.king.base.config.cloud;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author ππ
 * @date 2020-4-23 14:44
 * 云存储配置
 */
@Configuration
@ConfigurationProperties(prefix = "upload")
public class CloudConfig {

    /**
     * 云类型 1 阿里云  2 腾讯云  3 七牛云
     */
    private int type;
    /**
     * 域名
     */
    private String domain;
    /**
     * key
     */
    private String accessKey;
    /**
     * secret
     */
    private String secretKey;
    /**
     * 空间
     */
    private String bucket;
    /**
     * 文件夹
     */
    private String prefix;
    /**
     * 腾讯云appId
     */
    private String appId;
    /**
     *云存储所在区域
     */
    private String region;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
