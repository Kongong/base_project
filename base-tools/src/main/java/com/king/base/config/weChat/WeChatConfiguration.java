package com.king.base.config.weChat;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Spur
 * @date 2019/8/20
 * 企业微信
 */
@Configuration
@ConfigurationProperties(prefix = "wechat")
public class WeChatConfiguration {

    /**
     * 公司ID
     */
    private String corpId;

    /**
     * AgentId
     */
    private String agentId;

    /**
     * Secret
     */
    private String secret;

    /**
     * 获取token的链接
     */
    private String oauthAccessTokenUrl;

    /**
     * 获取授权用户信息
     */
    private String snsUserInfoUrl;

    /**
     * 获取jsapi_ticket
     * https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=ACCESS_TOKEN
     */
    private String getJsApiTicketUrl;

    /**
     * 发送消息
     */
    private String sendMsgUrl;

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getOauthAccessTokenUrl() {
        return oauthAccessTokenUrl;
    }

    public void setOauthAccessTokenUrl(String oauthAccessTokenUrl) {
        this.oauthAccessTokenUrl = oauthAccessTokenUrl;
    }

    public String getSnsUserInfoUrl() {
        return snsUserInfoUrl;
    }

    public void setSnsUserInfoUrl(String snsUserInfoUrl) {
        this.snsUserInfoUrl = snsUserInfoUrl;
    }

    public String getGetJsApiTicketUrl() {
        return getJsApiTicketUrl;
    }

    public void setGetJsApiTicketUrl(String getJsApiTicketUrl) {
        this.getJsApiTicketUrl = getJsApiTicketUrl;
    }

    public String getSendMsgUrl() {
        return sendMsgUrl;
    }

    public void setSendMsgUrl(String sendMsgUrl) {
        this.sendMsgUrl = sendMsgUrl;
    }
}
