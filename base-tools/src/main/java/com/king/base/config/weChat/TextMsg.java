package com.king.base.config.weChat;

import java.io.Serializable;

/**
 * 应用推送消息--文本消息，
 * @author Spur
 * @date 2019/8/20
 */
public class TextMsg extends BaseMsg implements Serializable {

    private static final long serialVersionUID = -4324476040184013733L;

    /**
     * 消息内容，最长不超过2048个字节
     */
    private Text text;

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }
}