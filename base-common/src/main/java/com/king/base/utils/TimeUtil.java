package com.king.base.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author TianYZ
 * @date 2019-06-15
 *
 * 日期工具
 */
public class TimeUtil {

    public static String getWeekDay(){
        String[] weekDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0){
            w = 0;
        }
        return weekDays[w];
    }

    public static List<String> getYearMonth(int number,String formatter) {
        List<String> result = new ArrayList<String>();
        try{
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MONTH, number);
            String before_six = c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH);//六个月前
            SimpleDateFormat sdf = new SimpleDateFormat(formatter);// 格式化为年月
            Calendar min = Calendar.getInstance();
            Calendar max = Calendar.getInstance();
            min.setTime(sdf.parse(before_six));
            min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);
            max.setTime(sdf.parse(sdf.format(new Date())));
            max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);
            Calendar curr = min;
            while (curr.before(max)) {
                result.add(sdf.format(curr.getTime()));
                curr.add(Calendar.MONTH, 1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }

//    public static void main(String[] args) {
//        getYearMonth(-4,"yyyy-MM");
//    }
}
