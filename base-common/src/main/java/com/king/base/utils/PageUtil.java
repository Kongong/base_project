package com.king.base.utils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页工具
 *
 * @author TianYZ
 * @date 2019-06-15
 */
public class PageUtil {


    /**
     * List 分页
     * @param page
     * @param size
     * @param list
     * @return
     */
    public static List toPage(int page, int size , List list) {
        int fromIndex = page * size;
        int toIndex = page * size + size;

        if(fromIndex > list.size()){
            return new ArrayList();
        } else if(toIndex >= list.size()) {
            return list.subList(fromIndex,list.size());
        } else {
            return list.subList(fromIndex,toIndex);
        }
    }

    /**
     * Page 数据处理，预防redis反序列化报错
     * @param page
     * @return
     */
    public static Map toPage(Page page) {
        Map map = new HashMap(2);
        map.put("data",page.getContent());
        map.put("total",page.getTotalElements());
        return map;
    }

    public static Map toPage(List list, long count) {
        Map map = new HashMap(2);
        map.put("data",list);
        map.put("total",count);
        return map;
    }

    public static Map toPage(IPage page) {
        Map map = new HashMap(2);
        map.put("data",page.getRecords());
        map.put("total",page.getTotal());
        return map;
    }
}
