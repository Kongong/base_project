package com.king.base.aop;

import java.lang.annotation.*;

/**
 * @author Spur
 * @date 2019-11-13
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScope {
    /**
     * 部门表的别名
     */
    public String deptAlias() default "";

    /**
     * 用户表的别名
     */
    public String userAlias() default "";
    /**
     * 自定义sql
     */
    public String scopeSql() default "";
}
