package com.king.base.msg;

/**
 * @author ππ
 * @date 2020-3-13 10:25
 */

public class BaseResponseEntity<T> extends BaseResponse {
    T data;

    public BaseResponseEntity() {
    }

    public BaseResponseEntity(T data) {
        this.data=data;
    }

    public BaseResponseEntity(Integer status, String message) {
        super(status, message);
    }

    public BaseResponseEntity(Integer status, String message, T data) {
        super(status, message);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
