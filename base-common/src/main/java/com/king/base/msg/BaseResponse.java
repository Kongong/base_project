package com.king.base.msg;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public class BaseResponse {

    private Integer status=200;
    private Boolean success;
    private String message;

    public BaseResponse() {
    }

    public BaseResponse(Integer status, String message) {
        this();
        this.status = status;
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        if(success==null){
            return getStatus()==200;
        }
        return success;
    }

    public void setSuccess(Boolean success) {
        if(success==null){
            success=getStatus()==200;
        }
        this.success = success;
    }
}


