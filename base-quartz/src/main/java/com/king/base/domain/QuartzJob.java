package com.king.base.domain;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.king.base.domain.base.BaseEntity;

import javax.validation.constraints.NotBlank;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@TableName("base_quartz_job")
public class QuartzJob extends BaseEntity {

    public static final String JOB_KEY = "JOB_KEY";
    @TableId
    private String jobId;

    @NotBlank
    private String beanName;

    @NotBlank
    private String cronExpression;

    private String isPause;

    private String jobName;

    @NotBlank
    private String methodName;

    private String params;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getIsPause() {
        return isPause;
    }

    public void setIsPause(String isPause) {
        this.isPause = isPause;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }
}