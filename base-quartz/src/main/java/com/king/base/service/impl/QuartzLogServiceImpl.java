package com.king.base.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.QuartzLog;
import com.king.base.mapper.QuartzLogMapper;
import com.king.base.service.QuartzLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@Service(value = "quartzLogService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class QuartzLogServiceImpl extends ServiceImpl<QuartzLogMapper,QuartzLog> implements QuartzLogService {
    

}
