package com.king.base.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.auth.utils.EntityUtils;
import com.king.base.msg.expection.BadRequestException;
import com.king.base.utils.PageUtil;
import com.king.base.utils.StringUtils;
import com.king.base.utils.ValidationUtil;
import com.king.base.domain.QuartzJob;
import com.king.base.mapper.QuartzJobMapper;
import com.king.base.service.QuartzJobService;
import com.king.base.utils.QuartzManage;
import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Optional;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@Service(value = "quartzJobService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class QuartzJobServiceImpl extends ServiceImpl<QuartzJobMapper,QuartzJob> implements QuartzJobService {
    
    @Autowired
    private QuartzManage quartzManage;

    @Override
    public QuartzJob findById(String id) {
        Optional<QuartzJob> quartzJob = Optional.ofNullable(baseMapper.selectById(id));
        ValidationUtil.isNull(quartzJob,"QuartzJob","id",id);
        return quartzJob.get();
    }

    @Override
    public QuartzJob create(QuartzJob resources) {
        if (!CronExpression.isValidExpression(resources.getCronExpression())){
            throw new BadRequestException("cron表达式格式错误");
        }
        EntityUtils.setCreateAndUpdateInfo(resources);
        baseMapper.insert(resources);
        quartzManage.addJob(resources);
        return resources;
    }

    @Override
    public void update(QuartzJob resources) {
        if(resources.getJobId().equals("1")){
            throw new BadRequestException("该任务不可操作");
        }
        if (!CronExpression.isValidExpression(resources.getCronExpression())){
            throw new BadRequestException("cron表达式格式错误");
        }
        EntityUtils.setUpdatedInfo(resources);
        baseMapper.updateById(resources);
        quartzManage.updateJobCron(resources);
    }

    @Override
    public void updateIsPause(QuartzJob quartzJob) {
        if(quartzJob.getJobId().equals("1")){
            throw new BadRequestException("该任务不可操作");
        }
        if ("0".equals(quartzJob.getIsPause())) {
            quartzManage.resumeJob(quartzJob);
            quartzJob.setIsPause("1");
        } else {
            quartzManage.pauseJob(quartzJob);
            quartzJob.setIsPause("0");
        }
        EntityUtils.setUpdatedInfo(quartzJob);
        baseMapper.updateById(quartzJob);
    }

    @Override
    public void execution(QuartzJob quartzJob) {
        if(quartzJob.getJobId().equals("1")){
            throw new BadRequestException("该任务不可操作");
        }
        quartzManage.runAJobNow(quartzJob);
    }

    @Override
    public Map queryAll(QuartzJob quartzJob, Pageable pageable) {
        IPage<QuartzJob> quartzJobIPage = baseMapper.selectPage(new Page<>(pageable.getPageNumber(), pageable.getPageSize()), Wrappers.<QuartzJob>lambdaQuery()
                .like(StringUtils.isNotBlank(quartzJob.getJobName()), QuartzJob::getJobName, quartzJob.getJobName()));
        return PageUtil.toPage(quartzJobIPage);
    }

    @Override
    public void delete(QuartzJob quartzJob) {
        if(quartzJob.getJobId().equals("1")){
            throw new BadRequestException("该任务不可操作");
        }
        quartzManage.deleteJob(quartzJob);
        baseMapper.deleteById(quartzJob);
    }
}
