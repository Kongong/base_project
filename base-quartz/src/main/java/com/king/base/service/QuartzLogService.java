package com.king.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.QuartzLog;

public interface QuartzLogService extends IService<QuartzLog> {
}
