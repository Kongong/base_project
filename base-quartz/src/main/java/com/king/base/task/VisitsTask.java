package com.king.base.task;
import com.king.base.service.visit.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@Component
public class VisitsTask {

    @Autowired
    private VisitService visitService;

    public void run(){
        visitService.save();
    }
}
