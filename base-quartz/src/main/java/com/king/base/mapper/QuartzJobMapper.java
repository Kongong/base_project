package com.king.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.QuartzJob;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public interface QuartzJobMapper extends BaseMapper<QuartzJob> {
}
