package com.king.base.rest;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.aop.Log;
import com.king.base.domain.QuartzJob;
import com.king.base.domain.QuartzLog;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.QuartzJobService;
import com.king.base.service.QuartzLogService;
import com.king.base.utils.PageUtil;
import com.king.base.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@RestController
@RequestMapping("api/quartz/job")
@Api(value = "job",description = "系统定时任务")
public class QuartzJobController {

    private static final String ENTITY_NAME = "quartzJob";
    private static final Logger log=LoggerFactory.getLogger(QuartzJobController.class);

    @Autowired
    private QuartzJobService quartzJobService;

    @Autowired
    private QuartzLogService quartzLogService;


    @Log(description = "查询定时任务")
    @GetMapping(value = "/page")
    @ApiOperation(value = "定时任务查询")
    public BaseResponseEntity getJobs(QuartzJob resources, Pageable pageable){
        return new BaseResponseEntity(quartzJobService.queryAll(resources,pageable));
    }

    /**
     * 查询定时任务日志
     * @param resources
     * @param pageable
     * @return
     */
    @Log(description = "定时任务日志查询")
    @ApiOperation(value = "定时任务日志查询")
    @GetMapping(value = "/pageLog")
    public BaseResponseEntity getJobLogs(QuartzLog resources, Pageable pageable){
        IPage<QuartzLog> quartzLogIPage = quartzLogService.page(new Page<>(pageable.getPageNumber(),pageable.getPageSize()),Wrappers.<QuartzLog>query()
                .eq(!ObjectUtils.isEmpty(resources.getSuccess()),"is_success",resources.getSuccess()!=null&& resources.getSuccess() ?1:0)
                .like(StringUtils.isNotBlank(resources.getJobName()),"job_name",resources.getJobName())
                .orderByDesc("crt_time"));
        return new BaseResponseEntity(PageUtil.toPage(quartzLogIPage));
    }

    @Log(description = "新增定时任务")
    @ApiOperation(value = "新增定时任务")
    @PostMapping(value = "/create")
    public BaseResponseEntity create(@Validated @RequestBody QuartzJob resources){
        if (resources.getJobId() != null) {
            return new BaseResponseEntity(HttpStatus.BAD_REQUEST.value(),"A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return new BaseResponseEntity(quartzJobService.create(resources));
    }

    @Log(description = "修改定时任务")
    @ApiOperation(value = "修改定时任务")
    @PutMapping(value = "/update")
    public BaseResponseEntity update(@Validated @RequestBody QuartzJob resources){
        if (resources.getJobId() == null) {
            return new BaseResponseEntity(HttpStatus.BAD_REQUEST.value(),ENTITY_NAME +" ID Can not be empty");
        }
        quartzJobService.update(resources);
        return new BaseResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log(description = "更改定时任务状态")
    @ApiOperation(value = "更改定时任务状态")
    @PutMapping(value = "/updateIsPause/{id}")
    public BaseResponseEntity updateIsPause(@PathVariable String id){
        quartzJobService.updateIsPause(quartzJobService.findById(id));
        return new BaseResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log(description = "执行定时任务")
    @ApiOperation(value = "执行定时任务")
    @PutMapping(value = "/exec/{id}")
    public BaseResponseEntity execution(@PathVariable String id){
        quartzJobService.execution(quartzJobService.findById(id));
        return new BaseResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log(description = "删除定时任务")
    @ApiOperation(value = "删除定时任务")
    @DeleteMapping(value = "/delete/{id}")
    public BaseResponseEntity delete(@PathVariable String id){
        quartzJobService.delete(quartzJobService.findById(id));
        return new BaseResponseEntity(HttpStatus.OK);
    }
    @GetMapping(value = "/getById/{jobId}")
    @ApiOperation(value = "根据ID查询定时任务")
    public BaseResponseEntity getById(@PathVariable String jobId){
        return new BaseResponseEntity(quartzJobService.getById(jobId));
    }
}
