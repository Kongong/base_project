##### 框架说明 base-project是一个前后台分离架构，此为后台模块。采用轻量级spring boot 2.0，持久层采用mybatis-plus,安全机制采用spring-security. 缓存模块采用redis及spring cache，API展示采用swagger，定时任务暂时使用spring quartz

1、后台模块均为restful模式，前后台交互采用JWT进行校验

2、base-common:系统公共模块，系统中公共方法；

3、base-core:系统核模块

4、base-monitor:系统监控模块

5、base-quartz:系统定时任务

6、base-flow：流程引擎，使用flowable 6.4.2(https://www.flowable.org/)

7、sql:数据库文件

8、前台连接地址https://gitee.com/tyz_0212/base_ui
地址：http://118.190.135.128:8080/ (账号:admin  密码:123456) 
服务器配置比较低

9、通过databaseId自动判断，支持mysql、sqlserver、oracle等主流数据库
 
10、swagger响应较慢，具体点击连接:http://118.190.135.128:8000/swagger-ui.html