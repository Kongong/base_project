package com.king.base.mapper.log;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.domain.log.Log;
import org.apache.ibatis.annotations.Param;

/**
 * @author Spur
 * @date 2019-10-28
 */
public interface LogMapper extends BaseMapper<Log> {
    IPage<Log> errorPage(Page page, @Param("log") Log log);
}
