package com.king.base.rest.server;
import com.king.base.domain.server.Server;
import com.king.base.msg.BaseResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 */
@RestController
@RequestMapping("api/monitor/server")
@Api(value = "server",description = "系统信息Controller")
public class ServerController{
    @GetMapping(value = "/getInfo")
    @ApiOperation(value = "查询服务器信息")
    public BaseResponseEntity getInfo() throws Exception
    {
        Server server = new Server();
        server.copyTo();
        return new BaseResponseEntity(server);
    }
}
