package com.king.base.rest.online;

import com.king.base.auth.service.OnlineService;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.utils.PageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

/**
 * @author Spur
 * @date 2019-11-14
 */
@RestController
@RequestMapping(value = "api/monitor/online")
@Api(value = "online",description = "在线用户Controller")
public class OnlineController {
    @Autowired
    private OnlineService onlineService;
    @ApiOperation(value = "分页查询")
    @GetMapping(value = "/page")
    public BaseResponseEntity page(String filter, Pageable pageable){
        Pageable  pg=new Pageable() {
            @Override
            public int getPageNumber() {
                return pageable.getPageNumber()-1;
            }

            @Override
            public int getPageSize() {
                return pageable.getPageSize();
            }

            @Override
            public long getOffset() {
                return 0;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public Pageable next() {
                return null;
            }

            @Override
            public Pageable previousOrFirst() {
                return null;
            }

            @Override
            public Pageable first() {
                return null;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }
        };
        return new BaseResponseEntity(PageUtil.toPage(onlineService.page(filter,pageable)));
    }
    @ApiOperation("踢出用户")
    @DeleteMapping(value = "/delete/{key}")
    public BaseResponseEntity delete(@PathVariable String key) throws Exception {
        onlineService.kickOut(key);
        return new BaseResponseEntity();
    }
}
