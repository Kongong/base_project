package com.king.base.rest.log;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.domain.log.Log;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.log.LogService;
import com.king.base.utils.PageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@RestController
@RequestMapping("api/monitor/log")
@Api(value = "log",description = "日志管理Controller")
public class LogController {

    @Autowired
    private LogService logService;

    @GetMapping(value = "/page")
    @ApiOperation(value = "日志分页查询")
    public BaseResponseEntity getLogs(Log log, Pageable pageable) {
        IPage<Log> loggingIPage = logService.page(new Page<>(pageable.getPageNumber(), pageable.getPageSize()), Wrappers.<Log>lambdaQuery()
                .like(StringUtils.isNotBlank(log.getCrtUserName()), Log::getCrtUserName, log.getCrtUserName())
                .eq(StringUtils.isNotBlank(log.getLogType()), Log::getLogType, log.getLogType())
                .like(StringUtils.isNoneBlank(log.getDescription()),Log::getDescription,log.getDescription())
                .orderByDesc(Log::getCrtTime));
        return new BaseResponseEntity(PageUtil.toPage(loggingIPage));
    }


    @GetMapping(value = "/error")
    @ApiOperation(value = "错误日志查询")
    public BaseResponseEntity getErrorLogs(Log log, Pageable pageable) {
        Page<Log> page=new Page<Log>(pageable.getPageNumber(),pageable.getPageSize());
        return new BaseResponseEntity(PageUtil.toPage(logService.errorPage(page,log)));
    }

    @GetMapping(value = "/errorById/{id}")
    @ApiOperation(value = "单个日志查询")
    public BaseResponseEntity getErrorLogsById(@PathVariable String id) {
        Log log=logService.getById(id);
        return new BaseResponseEntity(log);
    }
}
