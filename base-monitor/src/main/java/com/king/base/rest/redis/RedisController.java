package com.king.base.rest.redis;
import com.king.base.aop.Log;
import com.king.base.domain.redis.RedisVo;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.redis.RedisService;
import com.king.base.utils.PageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * @author TianYz
 */
@RestController
@RequestMapping("api/monitor/redis")
@Api(value = "redis",description = "Redis缓存Contoller")
public class RedisController {

    @Autowired
    private RedisService redisService;

    @Log(description = "查询Redis缓存")
    @ApiOperation(value = "redis缓存分页查询")
    @GetMapping(value = "/page")
    public BaseResponseEntity getRedis(String key, Pageable pageable){
        return new BaseResponseEntity(PageUtil.toPage(redisService.findByKey(key,pageable)));
    }
    @Log(description = "删除Redis缓存")
    @ApiOperation(value = "删除单个Redis缓存")
    @DeleteMapping(value = "/delete")
    public BaseResponseEntity delete(@RequestBody RedisVo resources){
        redisService.delete(resources.getKey());
        return new BaseResponseEntity(HttpStatus.OK);
    }

    @Log(description = "清空Redis缓存")
    @ApiOperation(value = "清空Redis缓存")
    @DeleteMapping(value = "/deleteAll")
    public BaseResponseEntity deleteAll(){
        redisService.flushdb();
        return new BaseResponseEntity(HttpStatus.OK);
    }
}
