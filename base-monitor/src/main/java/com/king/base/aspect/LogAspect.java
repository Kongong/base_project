package com.king.base.aspect;

import com.king.base.domain.log.Log;
import com.king.base.msg.expection.BadRequestException;
import com.king.base.service.log.LogService;
import com.king.base.utils.ThrowableUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author TianYZ
 * @date 2016-06-15
 */
@Component
@Aspect
public class LogAspect {

    @Autowired
    private LogService logService;

    private long currentTime = 0L;

    /**
     * 配置切入点
     */
    @Pointcut("@annotation(com.king.base.aop.Log)")
    public void logPointcut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }

    /**
     * 配置环绕通知,使用在方法logPointcut()上注册的切入点
     *
     * @param joinPoint join point for advice
     */
    @Around("logPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint){
        Object result = null;
        currentTime = System.currentTimeMillis();
        try {
            result = joinPoint.proceed();
        } catch (Throwable e) {
            throw new BadRequestException(e.getMessage());
        }
        Log log = new Log();
        log.setLogType("S");
        log.setResponseTime(System.currentTimeMillis() - currentTime);
//        EntityUtils.setCreateAndUpdateInfo(aspect);
        log.setCrtTime(new Date());
        logService.save(joinPoint, log);
        return result;
    }

    /**
     * 配置异常通知
     *
     * @param joinPoint join point for advice
     * @param e exception
     */
    @AfterThrowing(pointcut = "logPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        Log log = new Log();
        log.setLogType("F");
        log.setResponseTime(System.currentTimeMillis() - currentTime);
        log.setErrorDetail(ThrowableUtil.getStackTrace(e));
//        EntityUtils.setCreateAndUpdateInfo(aspect);
        logService.save((ProceedingJoinPoint)joinPoint, log);
    }
}
