package com.king.base.service.log;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.log.Log;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.scheduling.annotation.Async;

/**
 * @author Spur
 * @date 2019-10-28
 */
public interface LogService extends IService<Log> {
    /**
     * 新增日志
     * @param joinPoint
     * @param log
     */
    @Async
    void save(ProceedingJoinPoint joinPoint, Log log);


    /**
     * 异常日志查询
     * @param page
     * @param log
     */
    @Async
    IPage<Log> errorPage(Page page, Log log);
}
