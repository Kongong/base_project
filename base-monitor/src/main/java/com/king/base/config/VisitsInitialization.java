package com.king.base.config;
import com.king.base.service.visit.VisitService;
import org.springframework.context.annotation.Configuration;

/**
 * 初始化站点统计
 * @author TianYZ
 */
@Configuration
public class VisitsInitialization {

    public VisitsInitialization(VisitService visitService){
        System.out.println("--------------- 初始化站点统计，如果存在今日统计则跳过 ---------------");
        visitService.save();
    }
}
