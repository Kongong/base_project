package com.king.base.domain.role;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.king.base.domain.base.BaseEntity;

/**
 * 角色
 *
 * @author TianYZ
 * @date 2019-06-15
 */
@TableName("base_role")
public class Role extends BaseEntity {

    @TableId
    private String roleId; //角色ID

    private String roleCode; //角色编码

    private String roleName; //角色名称

    private Integer sort; //排序

    private String dataScope; //数据范围

    private String status; //角色状态

    private String scopeSql;// 自定义sql
    @TableField(exist = false)
    private String[] menuIds; //菜单ID
    @TableField(exist = false)
    private String[] deptIds; //部门ID

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getDataScope() {
        return dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String[] getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(String[] menuIds) {
        this.menuIds = menuIds;
    }

    public String[] getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(String[] deptIds) {
        this.deptIds = deptIds;
    }

    public String getScopeSql() {
        return scopeSql;
    }

    public void setScopeSql(String scopeSql) {
        this.scopeSql = scopeSql;
    }
}
