package com.king.base.domain.menu.vo;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public class MetaVo {

    private String title;

    private String icon;

    private boolean keepAlive;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public MetaVo(String title, String icon, boolean keepAlive) {
        this.title = title;
        this.icon = icon;
        this.keepAlive=keepAlive;
    }
    public boolean isKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(boolean keepAlive) {
        this.keepAlive = keepAlive;
    }
}
