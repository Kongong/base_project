package com.king.base.domain.user;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * @author Spur
 * @date 2019-11-5
 */
@TableName("base_user_role")
public class UserRole implements Serializable {
    private String userId;
    private String roleId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public UserRole(String userId, String roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }
}
