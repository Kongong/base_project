package com.king.base.domain.role;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * @author Spur
 * @date 2019-11-5
 */
@TableName("base_role_menu")
public class RoleMenu implements Serializable {
    private String roleId;
    private String menuId;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public RoleMenu(String roleId, String menuId) {
        this.roleId = roleId;
        this.menuId = menuId;
    }
}
