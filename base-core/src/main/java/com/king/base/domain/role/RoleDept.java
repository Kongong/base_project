package com.king.base.domain.role;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author Spur
 * @date 2019-11-12
 */
@TableName("base_role_dept")
public class RoleDept {
    private String roleId;
    private String deptId;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public RoleDept(String roleId, String deptId) {
        this.roleId = roleId;
        this.deptId = deptId;
    }
}
