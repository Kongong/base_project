package com.king.base.domain.serial;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author ππ
 * @date 2020-7-22 10:50
 * 生成流水码
 */
@TableName("base_serial_no")
public class SerialNo {
    /**
     * 主键
     */
    private int id;
    /**
     * 模块名称
     */
    private String module;
    /**
     * 前缀
     */
    private String prefix;
    /**
     * 年月日类型
     * 1: yyyyMMdd
     * 2: yyMMdd
     * 3: yyyyMM
     * 4: yyMM
     * 5: yyyy
     * 6: yy
     */
    private int mode;
    /**
     * 尾部流水码长度，例如4，尾部为0001
     */
    private int length;

    /**
     * 返回的流水码
     */
    private String newNo;

    public SerialNo(String module, String prefix, int mode,int length) {
        this.module = module;
        this.prefix = prefix;
        this.length = length;
        this.mode = mode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getNewNo() {
        return newNo;
    }

    public void setNewNo(String newNo) {
        this.newNo = newNo;
    }
}
