package com.king.base.rest.dict;
import com.king.base.aop.Log;
import com.king.base.domain.dict.DictValue;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.dict.DictValueService;
import com.king.base.utils.PageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * @author Spur
 * @date 2019/6/24
 */
@RestController
@RequestMapping("api/core/dictValue")
@Api(value = "dictValue",description = "字典值Controller")
public class DictValueController {
    @Autowired
    private DictValueService dictValueService;

    @GetMapping(value = "/page")
    @ApiOperation(value = "字典值分页查询")
    @Log(description = "字典值查询")
    public BaseResponseEntity page(Pageable pageable, DictValue dictValue){
        return new BaseResponseEntity(PageUtil.toPage(dictValueService.page(pageable,dictValue)));
    }

    @PostMapping(value = "/create")
    @ApiOperation(value = "创建")
    @Log(description = "字典值创建")
    public BaseResponseEntity create(@RequestBody DictValue entity){
        return dictValueService.createOrUpdate(entity);
    }

    @PutMapping(value = "/update")
    @ApiOperation(value = "更新")
    @Log(description = "字典值编辑")
    public BaseResponseEntity update(@RequestBody DictValue entity){
        return dictValueService.createOrUpdate(entity);
    }
    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "删除")
    @Log(description = "字典值删除")
    public BaseResponseEntity delete(@PathVariable String id){
        dictValueService.delete(id);
        return new BaseResponseEntity();
    }
    @GetMapping(value = "/getById/{id}")
    @ApiOperation(value = "根据ID查询")
    public BaseResponseEntity getById(@PathVariable String id){
        return new BaseResponseEntity(dictValueService.getById(id));
    }
}
