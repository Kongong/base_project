package com.king.base.rest.verify;
import com.king.base.auth.security.JwtUser;
import com.king.base.auth.utils.JwtTokenUtil;
import com.king.base.domain.verify.VerifyCode;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.verify.VerifyCodeService;
import com.king.base.utils.Constants;
import com.king.base.utils.RequestHolder;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

/**
 * @author TianYZ
 * @date 2019-06-16
 */
@RestController
@RequestMapping("api")
@Api(value = "code",description = "验证码")
public class VerifyCodeController {

    @Autowired
    private VerifyCodeService verifyCodeService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    @PostMapping(value = "/code/email/resetPass")
    public BaseResponseEntity resetPass() throws Exception {
        JwtUser jwtUser = (JwtUser)userDetailsService.loadUserByUsername(jwtTokenUtil.getLoginName(RequestHolder.getHttpServletRequest()));
        VerifyCode code = new VerifyCode();
        code.setType("email");
        code.setValue(jwtUser.getEmail());
        code.setScenes(Constants.RESET_MAIL);
//        EmailVo emailVo = verifyCodeService.sendEmail(code);
//        emailService.send(emailVo,emailService.find());
        return new BaseResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/code/validated")
    public BaseResponseEntity validated(VerifyCode code){
        verifyCodeService.validated(code);
        return new BaseResponseEntity(HttpStatus.OK);
    }
}
