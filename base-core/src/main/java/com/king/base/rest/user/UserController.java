package com.king.base.rest.user;

import com.king.base.aop.Log;
import com.king.base.auth.security.JwtUser;
import com.king.base.auth.utils.EncryptUtils;
import com.king.base.auth.utils.JwtTokenUtil;
import com.king.base.domain.user.User;
import com.king.base.domain.verify.VerifyCode;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.user.UserService;
import com.king.base.service.verify.VerifyCodeService;
import com.king.base.utils.Constants;
import com.king.base.utils.PageUtil;
import com.king.base.utils.RequestHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author TianYZ
 * @date 2019-06-16
 */
@RestController
@RequestMapping("api/core/user")
@Api(value = "user",description = "用户管理Controller")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

//    @Autowired
//    private PictureService pictureService;

    @Autowired
    private VerifyCodeService verifyCodeService;
    @GetMapping(value = "/getById/{userId}")
    @ApiOperation(value = "根据ID获取用户")
    public BaseResponseEntity getById(@PathVariable String userId){
        return new BaseResponseEntity(userService.findByUserId(userId));
    }

    @Log(description = "查询用户")
    @GetMapping(value = "/page")
    @ApiOperation(value = "用户分页查询")
    public BaseResponseEntity page(User user, Pageable pageable){
        return new BaseResponseEntity(PageUtil.toPage(userService.page(pageable,user)));
    }

    @Log(description = "新增用户")
    @PostMapping(value = "/create")
    @ApiOperation(value = "新建用户")
    public BaseResponseEntity create(@Validated @RequestBody User resources){
        return userService.createOrUpdate(resources);
    }

    @Log(description = "修改用户")
    @PutMapping(value = "/update")
    @ApiOperation(value = "修改用户")
    public BaseResponseEntity update(@Validated @RequestBody User resources){
        return userService.createOrUpdate(resources);
    }

    @Log(description = "删除用户")
    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "删除用户")
    public BaseResponseEntity delete(@PathVariable String id){
        userService.delete(id);
        return new BaseResponseEntity();
    }

    /**
     * 验证密码
     * @param
     * @return
     */
    @PostMapping(value = "/validPass")
    @ApiOperation(value = "校验验证码")
    public BaseResponseEntity validPass(@RequestBody  User user){
        JwtUser jwtUser = (JwtUser)userDetailsService.loadUserByUsername(jwtTokenUtil.getLoginName(RequestHolder.getHttpServletRequest()));
        Map map = new HashMap();
        map.put("status",200);
        if(!jwtUser.getPassword().equals(EncryptUtils.encryptPassword(user.getPassword()))){
           map.put("status",400);
        }
        return new BaseResponseEntity(map);
    }

    /**
     * 修改密码
     * @param
     * @return
     */
    @PostMapping(value = "/updatePass")
    @ApiOperation(value = "修改密码")
    public BaseResponseEntity updatePass(@RequestBody User user){
        JwtUser jwtUser = (JwtUser)userDetailsService.loadUserByUsername(jwtTokenUtil.getLoginName(RequestHolder.getHttpServletRequest()));
        if(jwtUser.getPassword().equals(EncryptUtils.encryptPassword(user.getPassword()))){
            return new BaseResponseEntity(HttpStatus.BAD_REQUEST.value(),"新密码不能与旧密码相同");
        }
        userService.updatePass(jwtUser,EncryptUtils.encryptPassword(user.getPassword()));
        return new BaseResponseEntity(HttpStatus.OK);
    }

    /**
     * 修改头像
     * @param file
     * @return
     */
//    @PostMapping(value = "/users/updateAvatar")
//    public ResponseEntity updateAvatar(@RequestParam MultipartFile file){
//        JwtUser jwtUser = (JwtUser)userDetailsService.loadUserByUsername(jwtTokenUtil.getUserName(RequestHolder.getHttpServletRequest()));
//        Picture picture = pictureService.upload(file,jwtUser.getUsername());
//        userService.updateAvatar(jwtUser,picture.getUrl());
//        return new ResponseEntity(HttpStatus.OK);
//    }

    /**
     * 修改邮箱
     * @param user
     * @param user
     * @return
     */
    @PostMapping(value = "/updateEmail/{code}")
    @ApiOperation(value = "修改邮箱")
    public BaseResponseEntity updateEmail(@PathVariable String code,@RequestBody User user){
        JwtUser jwtUser = (JwtUser)userDetailsService.loadUserByUsername(jwtTokenUtil.getLoginName(RequestHolder.getHttpServletRequest()));
        if(!jwtUser.getPassword().equals(EncryptUtils.encryptPassword(user.getPassword()))){
            return new BaseResponseEntity(HttpStatus.BAD_REQUEST.value(),"密码错误");
        }
        VerifyCode verifyCode = new VerifyCode(code, Constants.RESET_MAIL,"email",user.getEmail());
        verifyCodeService.validated(verifyCode);
        userService.updateEmail(jwtUser,user.getEmail());
        return new BaseResponseEntity(HttpStatus.OK);
    }
    @PutMapping(value = "/updateUserStatus")
    @ApiOperation(value = "更新用户状态")
    public BaseResponseEntity updateUserStatus(@RequestBody User user){
        userService.updateUserStatus(user);
        return new BaseResponseEntity(HttpStatus.OK);
    }
}
