package com.king.base.rest.post;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.aop.Log;
import com.king.base.domain.post.Post;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.utils.PageUtil;
import com.king.base.service.post.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * @author Spur
 * @date 2019/6/25
 */
@RestController
@RequestMapping(value = "api/core/post")
@Api(value = "post",description = "岗位管理Controller")
public class PostController {
    @Autowired
    private PostService postService;

    @GetMapping(value = "/page")
    @Log(description = "岗位查询")
    @ApiOperation(value = "岗位分页查询")
    public BaseResponseEntity page(Post post, Pageable pageable){
        Page<Post> page=new Page<Post>(pageable.getPageNumber(),pageable.getPageSize());
        return new BaseResponseEntity(PageUtil.toPage(postService.selectByPage(page, post)));
    }
    @GetMapping(value = "/getById/{postId}")
    @ApiOperation(value = "根据ID查询岗位")
    public BaseResponseEntity getById(@PathVariable String postId){
        return new BaseResponseEntity(postService.getById(postId));
    }
    @PostMapping(value = "/create")
    @Log(description = "岗位创建")
    @ApiOperation(value = "岗位创建")
    public BaseResponseEntity create(@RequestBody Post post){
        return postService.create(post);
    }
    @PutMapping(value = "/update")
    @Log(description = "岗位编辑")
    @ApiOperation(value = "岗位编辑")
    public BaseResponseEntity update(@RequestBody Post post){

        return postService.update(post);
    }
    @DeleteMapping(value = "/delete/{id}")
    @Log(description = "岗位删除")
    @ApiOperation(value = "岗位删除")
    public BaseResponseEntity delete(@PathVariable String id){
        postService.removeById(id);
        return new BaseResponseEntity();
    }
}
