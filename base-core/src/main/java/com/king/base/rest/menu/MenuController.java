package com.king.base.rest.menu;
import com.king.base.aop.Log;
import com.king.base.auth.utils.JwtTokenUtil;
import com.king.base.domain.menu.Menu;
import com.king.base.domain.user.User;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.menu.MenuService;
import com.king.base.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author TianYZ
 * @date 2019-06-16
 */
@RestController
@RequestMapping("api/core/menu")
@Api(value = "menu",description = "菜单管理Controller")
public class MenuController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private MenuService menuService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/getById/{id}")
    @ApiOperation(value = "根据ID获取菜单")
    public BaseResponseEntity getById(@PathVariable String id){
        return new BaseResponseEntity(menuService.getById(id));
    }
    @GetMapping(value = "/buildMenuRouter")
    @ApiOperation(value = "构建左侧菜单")
    public BaseResponseEntity buildMenuRouter(HttpServletRequest request){
        User user = userService.findByName(jwtTokenUtil.getLoginName(request));
        List<Menu> menuList = menuService.selectMenuByUserId(user.getUserId());
        return new BaseResponseEntity(menuService.buildMenuRouter(menuList));
    }
    @Log(description = "查询菜单")
    @GetMapping(value = "/list")
    @ApiOperation(value = "菜单列表")
    public BaseResponseEntity list(Menu menu){
        List<Menu> menus=menuService.selectMenuList(menu);
        return new BaseResponseEntity(menuService.buildMenuTree(menus));
    }
    @GetMapping(value = "/tree")
    @ApiOperation(value = "下拉菜单选择器")
    public BaseResponseEntity tree(Menu menu){
        List<Menu> menus=menuService.selectMenuList(menu);
        return new BaseResponseEntity(menuService.buildMenuTreeSelect(menus));
    }
    @Log(description = "新增菜单")
    @PostMapping(value = "/create")
    @ApiOperation(value = "新建菜单")
    public BaseResponseEntity create(@Validated @RequestBody Menu menu){

        return menuService.createOrUpdate(menu);
    }

    @Log(description = "修改菜单")
    @PutMapping(value = "/update")
    @ApiOperation(value = "修改菜单")
    public BaseResponseEntity update(@Validated @RequestBody Menu menu){
        return menuService.createOrUpdate(menu);
    }

    @Log(description = "删除菜单")
    @ApiOperation(value = "删除菜单")
    @DeleteMapping(value = "/delete/{menuId}")
    public BaseResponseEntity delete(@PathVariable String menuId){
        menuService.delete(menuId);
        return new BaseResponseEntity();
    }
    @GetMapping(value = "/selectMenuByRoleId/{roleId}")
    @ApiOperation(value = "根据角色ID查询菜单")
    public BaseResponseEntity selectMenuByRoleId(@PathVariable String roleId){
        return new BaseResponseEntity(menuService.selectMenuByRoleId(roleId));
    }
}
