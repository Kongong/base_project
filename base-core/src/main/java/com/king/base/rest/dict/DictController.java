package com.king.base.rest.dict;
import com.king.base.aop.Log;
import com.king.base.domain.dict.DictType;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.utils.PageUtil;
import com.king.base.service.dict.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * @author Spur
 * @date 2019/6/24
 */
@RestController
@RequestMapping(value = "api/core/dict")
@Api(value = "dict",description = "字典管理Controller")
public class DictController {
    @Autowired
    private DictService dictService;

    @GetMapping(value = "/page")
    @ApiOperation(value = "分页列表查询")
    @Log(description = "查询字典")
    public BaseResponseEntity page(DictType dictType, Pageable pageable){
        return new BaseResponseEntity(PageUtil.toPage(dictService.page(pageable, dictType)));
    }

    @PostMapping(value = "/create")
    @ApiOperation(value = "字典创建")
    @Log(description = "字典创建")
    public BaseResponseEntity create(@RequestBody DictType dictType){

        return dictService.createOrUpdate(dictType);
    }
    @PutMapping(value = "/update")
    @ApiOperation(value = "字典编辑")
    @Log(description = "字典编辑")
    public BaseResponseEntity update(@RequestBody DictType dictType){
        return dictService.createOrUpdate(dictType);
    }

    @DeleteMapping(value = "/delete/{dictId}/{dictCode}")
    @ApiOperation(value = "字典删除")
    @Log(description = "字典删除")
    public BaseResponseEntity delete(@PathVariable String dictId,@PathVariable String dictCode){
        dictService.delete(dictId,dictCode);
        return new BaseResponseEntity();
    }
    @GetMapping(value = "/getById/{dictId}")
    @ApiOperation(value = "获取单个字典")
    public BaseResponseEntity getById(@PathVariable String dictId){
        return new BaseResponseEntity(dictService.getById(dictId));
    }
}
