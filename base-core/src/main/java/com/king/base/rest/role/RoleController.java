package com.king.base.rest.role;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.aop.Log;
import com.king.base.domain.role.Role;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.role.RoleService;
import com.king.base.utils.PageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author TianYZ
 * @date 2019-06-16
 */
@RestController
@RequestMapping("api/core/role")
@Api(value = "role",description = "角色管理Controller")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @GetMapping(value = "/getById/{roleId}")
    @ApiOperation(value = "通过ID获取角色")
    public BaseResponseEntity getById(@PathVariable String roleId){
        return new BaseResponseEntity(roleService.findById(roleId));
    }
    @GetMapping(value = "/tree")
    @ApiOperation(value = "前台下拉选择")
    public BaseResponseEntity getRoleTree(){
        return new BaseResponseEntity(roleService.getRoleTree());
    }

    @Log(description = "查询角色")
    @GetMapping(value = "/page")
    @ApiOperation(value = "分页查询")
    public BaseResponseEntity page(Role role, Pageable pageable){
        Page<Role> page=new Page<Role>(pageable.getPageNumber(),pageable.getPageSize());
        return new BaseResponseEntity(PageUtil.toPage(roleService.page(page,role)));
    }

    @Log(description = "新增角色")
    @PostMapping(value = "/create")
    @ApiOperation(value = "新增")
    public BaseResponseEntity create(@Validated @RequestBody Role resources){
        return roleService.createOrUpdate(resources);
    }

    @Log(description = "修改角色")
    @PutMapping(value = "/update")
    @ApiOperation(value = "修改")
    public BaseResponseEntity update(@Validated @RequestBody Role resources){
        return roleService.createOrUpdate(resources);
    }

    @Log(description = "删除角色")
    @DeleteMapping(value = "/delete/{roleId}")
    @ApiOperation(value = "删除")
    public BaseResponseEntity delete(@PathVariable String roleId){
        return roleService.delete(roleId);
    }
    @Log(description = "修改角色状态")
    @PutMapping(value = "/updateRoleStatus")
    @ApiOperation(value = "修改角色状态")
    public BaseResponseEntity updateRoleStatus(@RequestBody Role role){
        roleService.updateRoleStatus(role);
        return new BaseResponseEntity();
    }
    @GetMapping(value = "/getRolesByUserId/{userId}")
    @ApiOperation(value = "根据用户ID查询角色")
    public BaseResponseEntity getRolesByUserId(@PathVariable String userId){
        return new BaseResponseEntity(roleService.getRoleIdsByUserId(userId));
    }

    @PostMapping(value = "/grantDataScope")
    @ApiOperation(value = "分配数据权限")
    @Log(description = "分配数据权限")
    public BaseResponseEntity grantDataScope(@RequestBody Role role){
        roleService.grantDataScope(role);
        return new BaseResponseEntity();
    }

    @GetMapping(value = "/selectDeptIdByRoleId/{roleId}")
    @ApiOperation(value = "根据角色查询分配部门")
    public BaseResponseEntity selectDeptIdByRoleId(@PathVariable String roleId){
        return new BaseResponseEntity(roleService.selectDeptIdByRoleId(roleId));
    }
}
