package com.king.base.rest.visit;

import com.king.base.msg.BaseResponseEntity;
import com.king.base.service.visit.VisitService;
import com.king.base.utils.RequestHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@RestController
@RequestMapping("api/core/visit")
@Api(value = "visit",description = "访客记录Controller")
public class VisitController {

    @Autowired
    private VisitService visitService;

    @PostMapping(value = "/create")
    @ApiOperation(value = "创建")
    public BaseResponseEntity create(){
        visitService.count(RequestHolder.getHttpServletRequest());
        return new BaseResponseEntity();
    }

    @GetMapping(value = "/get")
    @ApiOperation(value = "查询")
    public BaseResponseEntity get(){
        return new BaseResponseEntity(visitService.get());
    }

    @GetMapping(value = "/chartData")
    @ApiOperation(value = "首页图表")
    public BaseResponseEntity getChartData(){
        return new BaseResponseEntity(visitService.getChartData());
    }
}
