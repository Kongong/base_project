package com.king.base.service.dict;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.dict.DictType;
import com.king.base.msg.BaseResponseEntity;
import org.springframework.data.domain.Pageable;

/**
 * @author Spur
 * @date 2019/6/24
 */
public interface DictService extends IService<DictType> {

    IPage<DictType> page(Pageable pageable, DictType dictType);

    boolean checkUnique(DictType dictType);

    void delete(String id,String dictCode);

    BaseResponseEntity createOrUpdate(DictType dictType);
}
