package com.king.base.service.user.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.user.UserRole;
import com.king.base.mapper.user.UserRoleMapper;
import com.king.base.service.user.UserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Spur
 * @date 2019-11-7
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper,UserRole> implements UserRoleService {
    @Override
    public void deleteUserRole(String userId, String roleId) {
        baseMapper.deleteUserRole(userId,roleId);
    }
}
