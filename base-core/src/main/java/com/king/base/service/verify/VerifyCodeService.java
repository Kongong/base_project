package com.king.base.service.verify;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.verify.VerifyCode;

/**
 * @author TianYZ
 * @date 2019-06-16
 */
public interface VerifyCodeService extends IService<VerifyCode> {

    /**
     * 发送邮件验证码
     * @param code
     */
//    EmailVo sendEmail(VerifyCode code);

    /**
     * 验证
     * @param code
     */
    void validated(VerifyCode code);
}
