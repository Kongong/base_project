package com.king.base.service.role.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.role.RoleMenu;
import com.king.base.mapper.role.RoleMenuMapper;
import com.king.base.service.role.RoleMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Spur
 * @date 2019-11-7
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper,RoleMenu> implements RoleMenuService {
    @Override
    public boolean checkMenuExistRole(String menuId) {
        return baseMapper.checkMenuExistRole(menuId)>0;
    }

    @Override
    public void deleteRoleMenuByRoleId(String roleId) {
        baseMapper.deleteRoleMenuByRoleId(roleId);
    }
}
