package com.king.base.service.role;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.role.Role;
import com.king.base.msg.BaseResponseEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@CacheConfig(cacheNames = "role")
public interface RoleService extends IService<Role> {

    /**
     * get
     * @param id
     * @return
     */
    Role findById(String id);

    /**
     * 分页查询
     * @param page
     * @param role
     * @return
     */
    IPage<Role> page(Page<Role> page, @Param("bean") Role role);

    /**
     * delete
     * @param id
     */
    @CacheEvict(key = "'roleTree'")
    BaseResponseEntity delete(String id);

    /**
     * role tree
     * @return
     */
    @Cacheable(key = "'roleTree'")
    List<Role> getRoleTree();

    /**
     * 通过登录用户查找角色
     * @param userId
     * @return
     */
    List<Role> findByUserId(String userId);

    void updateRoleStatus(Role role);

    List<String> getRoleIdsByUserId(String userId);

    @CacheEvict(key = "'roleTree'")
    BaseResponseEntity createOrUpdate(Role entity);

    void grantDataScope(Role role);

    List<String> selectDeptIdByRoleId(String roleId);
}
