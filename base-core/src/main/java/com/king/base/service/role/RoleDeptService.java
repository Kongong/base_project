package com.king.base.service.role;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.role.RoleDept;

import java.util.List;

/**
 * @author Spur
 * @date 2019-11-13
 */
public interface RoleDeptService extends IService<RoleDept> {
    boolean deleteRoleDeptByRoleId(String roleId);

    List<String> selectDeptIdByRoleId(String roleId);
}
