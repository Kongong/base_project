package com.king.base.service.dict.impl;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.auth.utils.EntityUtils;
import com.king.base.domain.dict.DictValue;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.mapper.dict.DictValueMapper;
import com.king.base.service.dict.DictValueService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Spur
 * @date 2019/6/24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DictValueServiceImpl extends ServiceImpl<DictValueMapper,DictValue> implements DictValueService {
    @Override
    public IPage<DictValue> page(Pageable pageable, DictValue bean) {
        Page<DictValue> page=new Page<DictValue>(pageable.getPageNumber(),pageable.getPageSize());
        return page.setRecords(baseMapper.page(page,bean));
    }
    //根据字典ID删除所有值明细
    @Override
    public void deleteByDictCode(String dictCode) {
        baseMapper.deleteByDictCode(dictCode);
    }
    @Override
    public void delete(String id) {
        baseMapper.deleteById(id);
    }

    @Override
    public BaseResponseEntity createOrUpdate(DictValue entity) {
        if(baseMapper.checkUniqueValue(entity)>0){
            return new BaseResponseEntity(HttpStatus.BAD_REQUEST.value(),"字典值重复:"+entity.getValue());
        }
        EntityUtils.setCreateAndUpdateInfo(entity);
        super.saveOrUpdate(entity);
        return new BaseResponseEntity();
    }
}
