package com.king.base.service.menu;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.common.TreeSelect;
import com.king.base.domain.menu.Menu;
import com.king.base.domain.menu.vo.MenuVo;
import com.king.base.msg.BaseResponseEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Set;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@CacheConfig(cacheNames = "menu")
public interface MenuService extends IService<Menu> {

    /**
     * 查询系统全部菜单
     * @param menu
     * @return
     */
    @Cacheable(key = "'menuAll'")
    List<Menu> selectMenuList(Menu menu);
    /**
     * 根据用户ID查询操作权限
     * @param userId
     * @return
     */
    Set<String> selectMenuPermissionByUserId(String userId);

    /**
     * 根据用户ID构建菜单
     * @return
     */
    List<Menu> selectMenuByUserId(String userId);

    /**
     * 根据角色查询菜单
     * @param roleId
     * @return
     */
    List<String> selectMenuByRoleId(String roleId);

    /**
     * 构建前台所需路由
     * @param menus
     * @return
     */
    List<MenuVo> buildMenuRouter(List<Menu> menus);

    /**
     * 构建前端所需要树结构
     *
     * @param menus 菜单列表
     * @return 树结构列表
     */
    List<Menu> buildMenuTree(List<Menu> menus);

    /**
     * 构建菜单选择器
     * @param menus
     * @return
     */
    List<TreeSelect> buildMenuTreeSelect(List<Menu> menus);

    boolean checkMenuNameUnique(Menu menu);

    boolean hasChildByMenuId(String menuId);
    @CacheEvict(key = "'menuAll'")
    void delete(String menuId);
    @CacheEvict(key = "'menuAll'")
    BaseResponseEntity createOrUpdate(Menu entity);
}
