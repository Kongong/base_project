package com.king.base.service.dict;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.dict.DictValue;
import com.king.base.msg.BaseResponseEntity;
import org.springframework.data.domain.Pageable;

/**
 * @author Spur
 * @date 2019/6/24
 */
public interface DictValueService extends IService<DictValue> {

    IPage<DictValue> page(Pageable pageable,DictValue bean);

    void deleteByDictCode(String dictId);

    void delete(String id);

    BaseResponseEntity createOrUpdate(DictValue bean);
}
