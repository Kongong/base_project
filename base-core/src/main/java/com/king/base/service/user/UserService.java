package com.king.base.service.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.auth.security.JwtUser;
import com.king.base.domain.user.User;
import com.king.base.msg.BaseResponseEntity;
import org.springframework.data.domain.Pageable;

/**
 * @author TianYZ
 * @date 2019-06-16
 */
public interface UserService extends IService<User> {

    /**
     * get
     * @param userId
     * @return
     */
    User findByUserId(String userId);

    /**
     * 分页查询
     * @param user
     * @param pageable
     * @return
     */
    IPage<User> page(Pageable pageable, User user);

    /**
     * delete
     * @param id
     */
    void delete(String id);

    User findByName(String userName);

    /**
     * 修改密码
     * @param jwtUser
     * @param encryptPassword
     */
    void updatePass(JwtUser jwtUser, String encryptPassword);

    /**
     * 修改头像
     * @param jwtUser
     * @param url
     */
    void updateAvatar(JwtUser jwtUser, String url);

    /**
     * 修改邮箱
     * @param jwtUser
     * @param email
     */
    void updateEmail(JwtUser jwtUser, String email);

    void updateUserStatus(User user);

    BaseResponseEntity createOrUpdate(User user);
}
