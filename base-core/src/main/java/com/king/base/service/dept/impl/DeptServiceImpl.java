package com.king.base.service.dept.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.auth.utils.EntityUtils;
import com.king.base.common.TreeSelect;
import com.king.base.domain.dept.Dept;
import com.king.base.mapper.dept.DeptMapper;
import com.king.base.service.dept.DeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Spur
 * @date 2019/6/24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DeptServiceImpl extends ServiceImpl<DeptMapper,Dept> implements DeptService {
    /**
     * 查询部门集合
     * @param dept
     * @return
     */
    @Override
    public List<Dept> selectDeptList(Dept dept) {
        return baseMapper.selectDeptList(dept);
    }

    /**
     * 构建前台部门树形列表
     * @param deptList
     * @return
     */
    @Override
    public List<Dept> buildDeptTree(List<Dept> deptList) {
        List<Dept> returnList = new ArrayList<Dept>();
        for (Iterator<Dept> iterator = deptList.iterator(); iterator.hasNext();)
        {
            Dept t = (Dept) iterator.next();
            // 根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() .equals("0"))
            {
                recursion(deptList, t);
                returnList.add(t);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = deptList;
        }
        return returnList;
    }

    /**
     * 构建部门下拉属性选择器
     * @param deptList
     * @return
     */
    @Override
    public List<TreeSelect> buildDeptTreeSelect(List<Dept> deptList) {
        List<Dept> deptTrees=buildDeptTree(deptList);
        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }
    /**
     * 递归列表
     */
    private void recursion(List<Dept> list, Dept t){
        // 得到子节点列表
        List<Dept> childList = getChildList(list, t);
        t.setChildren(childList);
        for (Dept tChild : childList){
            if (hasChild(list, tChild)){
                // 判断是否有子节点
                Iterator<Dept> it = childList.iterator();
                while (it.hasNext()){
                    Dept n = (Dept) it.next();
                    recursion(list, n);
                }
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<Dept> getChildList(List<Dept> list, Dept t){
        List<Dept> childList = new ArrayList<Dept>();
        Iterator<Dept> it = list.iterator();
        while (it.hasNext()){
            Dept n = (Dept) it.next();
            if (n.getParentId().equals(t.getDeptId()) ){
                childList.add(n);
            }
        }
        return childList;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<Dept> list, Dept t)
    {
        return getChildList(list, t).size() > 0 ;
    }

    /**
     * 根据部门ID判断是否存在子节点
     * @param deptId 部门ID
     * @return
     */
    @Override
    public boolean hasChildByDeptId(String deptId) {
        return baseMapper.hasChildByDeptId(deptId)>0;
    }

    /**
     * 判断部门下，是否存在人员
     * @param deptId 部门ID
     * @return
     */
    @Override
    public boolean checkDeptExistUser(String deptId) {
        return baseMapper.checkDeptExistUser(deptId)>0;
    }

    /**
     * 判断同一上级部门下，名称是否重复
     * @param dept 部门信息
     * @return
     */
    @Override
    public boolean checkDeptNameUnique(Dept dept) {
        return baseMapper.checkDeptNameUnique(dept)>0;
    }

    @Override
    public boolean saveOrUpdate(Dept entity) {
        if(StringUtils.isEmpty(entity.getDeptId())){
            EntityUtils.setCreateAndUpdateInfo(entity);
        }else{
            EntityUtils.setUpdatedInfo(entity);
        }
        return super.saveOrUpdate(entity);
    }

    @Override
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }
}
