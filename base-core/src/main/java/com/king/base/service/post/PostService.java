package com.king.base.service.post;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.post.Post;
import com.king.base.msg.BaseResponseEntity;

/**
 * @author Spur
 * @date 2019/6/25
 */
public interface PostService extends IService<Post> {

    IPage<Post> selectByPage(Page<Post> page, Post post);

    BaseResponseEntity update(Post entity);

    BaseResponseEntity create(Post entity);

    boolean checkUniqueName(Post post);
}
