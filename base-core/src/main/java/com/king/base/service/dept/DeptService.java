package com.king.base.service.dept;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.common.TreeSelect;
import com.king.base.domain.dept.Dept;
import java.util.List;

/**
 * @author Spur
 * @date 2019/6/24
 */

public interface DeptService extends IService<Dept> {

    /**
     * 根据条件查询部门
     * @param dept
     * @return
     */
    List<Dept> selectDeptList(Dept dept);

    /**
     * 构建菜单树形列表
     * @param deptList
     * @return
     */
    List<Dept> buildDeptTree(List<Dept> deptList);

    /**
     * 构建部门下拉树形选择器
     * @param deptList
     * @return
     */
    List<TreeSelect> buildDeptTreeSelect(List<Dept> deptList);
    /**
     * 是否存在部门子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    boolean hasChildByDeptId(String deptId);

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    boolean checkDeptExistUser(String deptId);

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    boolean checkDeptNameUnique(Dept dept);
}
