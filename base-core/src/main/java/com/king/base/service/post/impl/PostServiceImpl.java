package com.king.base.service.post.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.post.Post;
import com.king.base.msg.BaseResponseEntity;
import com.king.base.mapper.post.PostMapper;
import com.king.base.service.post.PostService;
import com.king.base.auth.utils.EntityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * @author Spur
 * @date 2019/6/25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PostServiceImpl extends ServiceImpl<PostMapper,Post> implements PostService {
    @Override
    public IPage<Post> selectByPage(Page<Post> page, Post post) {
        return page.setRecords(baseMapper.selectByPage(page, post));
    }

    @Override
    public BaseResponseEntity create(Post entity) {
        if(checkUniqueName(entity)){
            return new BaseResponseEntity(HttpStatus.BAD_REQUEST.value(),"岗位名称重复:"+entity.getPostName());
        }
        EntityUtils.setCreateAndUpdateInfo(entity);
        baseMapper.insert(entity);
        return new BaseResponseEntity();
    }

    @Override
    public BaseResponseEntity update(Post entity) {
        if(checkUniqueName(entity)){
            return new BaseResponseEntity(HttpStatus.BAD_REQUEST.value(),"岗位名称重复:"+entity.getPostName());
        }
        EntityUtils.setUpdatedInfo(entity);
        baseMapper.updateById(entity);
        return new BaseResponseEntity();
    }

    @Override
    public Post getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    public boolean checkUniqueName(Post post) {
        return baseMapper.checkUniqueName(post)>0;
    }
}
