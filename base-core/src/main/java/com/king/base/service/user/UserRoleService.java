package com.king.base.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.user.UserRole;

/**
 * @author Spur
 * @date 2019-11-7
 */
public interface UserRoleService extends IService<UserRole> {

    void deleteUserRole(String userId,String roleId);
}
