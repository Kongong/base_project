package com.king.base.service.visit;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.visit.Visit;
import org.springframework.scheduling.annotation.Async;

import javax.servlet.http.HttpServletRequest;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public interface VisitService extends IService<Visit>{

    /**
     * 提供给定时任务，每天0点执行
     */
    void save();

    /**
     * 新增记录
     * @param request
     */
    @Async
    void count(HttpServletRequest request);

    /**
     * 获取数据
     * @return
     */
    Object get();

    /**
     * getChartData
     * @return
     */
    Object getChartData();
}
