package com.king.base.service.role;

import com.baomidou.mybatisplus.extension.service.IService;
import com.king.base.domain.role.RoleMenu;

/**
 * @author Spur
 * @date 2019-11-7
 */
public interface RoleMenuService extends IService<RoleMenu> {

    boolean checkMenuExistRole(String menuId);

    void deleteRoleMenuByRoleId(String roleId);
}
