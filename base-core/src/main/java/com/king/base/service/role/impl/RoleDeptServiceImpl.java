package com.king.base.service.role.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.role.RoleDept;
import com.king.base.mapper.role.RoleDeptMapper;
import com.king.base.service.role.RoleDeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Spur
 * @date 2019-11-13
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RoleDeptServiceImpl extends ServiceImpl<RoleDeptMapper,RoleDept> implements RoleDeptService {
    @Override
    public boolean deleteRoleDeptByRoleId(String roleId) {
        baseMapper.deleteRoleDeptByRoleId(roleId);
        return true;
    }

    @Override
    public List<String> selectDeptIdByRoleId(String roleId) {
        return baseMapper.selectDeptIdByRoleId(roleId);
    }
}
