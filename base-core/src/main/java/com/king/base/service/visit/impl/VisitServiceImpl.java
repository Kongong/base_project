package com.king.base.service.visit.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.king.base.domain.visit.Visit;
import com.king.base.mapper.logger.LoggerMapper;
import com.king.base.mapper.visit.VisitMapper;
import com.king.base.service.visit.VisitService;
import com.king.base.utils.TimeUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisitServiceImpl extends ServiceImpl<VisitMapper,Visit> implements VisitService {
    

    @Resource
    private LoggerMapper loggerMapper;

    @Override
    public void save() {
        LocalDate localDate = LocalDate.now();
        Visit visit = baseMapper.findByDate(localDate.toString());
        if(visit == null){
            visit = new Visit();
            visit.setWeekDay(TimeUtil.getWeekDay());
            visit.setPvCounts(1L);
            visit.setIpCounts(1L);
            visit.setDate(localDate.toString());
//            EntityUtils.setCreateAndUpdateInfo(visit);
            visit.setCrtTime(new Date());
            baseMapper.insert(visit);
        }
    }

    @Override
    public void count(HttpServletRequest request) {
        LocalDate localDate = LocalDate.now();
        Visit visit = baseMapper.findByDate(localDate.toString());
        visit.setPvCounts(visit.getPvCounts()+1);
        long ipCounts = loggerMapper.findIp(localDate.toString(), localDate.plusDays(1).toString());
        visit.setIpCounts(ipCounts);
        //EntityUtils.setUpdatedInfo(visit);
        baseMapper.updateById(visit);
    }

    @Override
    public Object get() {
        Map map = new HashMap();
        LocalDate localDate = LocalDate.now();
        Visit visit = baseMapper.findByDate(localDate.toString());
        List<Visit> list = baseMapper.findAllVisits(localDate.minusDays(6).toString(),localDate.plusDays(1).toString());

        long recentVisits = 0, recentIp = 0;
        for (Visit data : list) {
            recentVisits += data.getPvCounts();
            recentIp += data.getIpCounts();
        }
        map.put("newVisits", visit.getPvCounts());
        map.put("newIp", visit.getIpCounts());
        map.put("recentVisits",recentVisits);
        map.put("recentIp",recentIp);
        return map;
    }

    @Override
    public Object getChartData() {
        Map map = new HashMap();
        LocalDate localDate = LocalDate.now();
        List<Visit> list = baseMapper.findAllVisits(localDate.minusDays(6).toString(),localDate.plusDays(1).toString());
        map.put("weekDays",list.stream().map(Visit::getWeekDay).collect(Collectors.toList()));
        map.put("visitsData",list.stream().map(Visit::getPvCounts).collect(Collectors.toList()));
        map.put("ipData",list.stream().map(Visit::getIpCounts).collect(Collectors.toList()));
        return map;
    }
}
