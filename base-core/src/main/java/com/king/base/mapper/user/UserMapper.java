package com.king.base.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.domain.user.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface UserMapper extends BaseMapper<User> {
    /**
     * findByUsername
     * @param user
     * @return
     */
    int  checkLoginNameUnique(User user);

    /**
     * 通过登录名查询
     * @param loginName
     * @return
     */
    User findByLoginName(@Param("loginName")String loginName);

    /**
     * 条件分页查询
     * @param user
     * @return
     */
    List<User> page(Page<User> page, @Param("bean") User user);

    /**
     * findByEmail
     * @param email
     * @return
     */
    User findByEmail(@Param("email") String email);

    /**
     * 修改密码
     * @param pass
     */
    void updatePass(@Param("userId") String userId, @Param("pass") String pass);

    /**
     * 修改头像
     * @param url
     */
    void updateAvatar(@Param("userId") String userId, @Param("url") String url);

    /**
     * 修改邮箱
     * @param userId
     * @param email
     */
    void updateEmail(@Param("userId") String userId, @Param("email") String email);



    User findByUserId(String userId);
}
