package com.king.base.mapper.dict;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.domain.dict.DictType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Spur
 * @date 2019/6/24
 */
public interface DictTypeMapper extends BaseMapper<DictType> {

    List<DictType> page(Page<DictType> page, @Param("bean") DictType dictType);

    int checkUniqueCode(DictType dictType);
}
