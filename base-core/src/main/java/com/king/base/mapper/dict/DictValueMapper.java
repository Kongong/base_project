package com.king.base.mapper.dict;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.domain.dict.DictValue;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Spur
 * @date 2019/6/24
 */
public interface DictValueMapper extends BaseMapper<DictValue> {

    List<DictValue> page(Page<DictValue> page, @Param("bean") DictValue dictValue);

    void deleteByDictCode(String dictCode);

    int checkUniqueValue(DictValue dictValue);
}
