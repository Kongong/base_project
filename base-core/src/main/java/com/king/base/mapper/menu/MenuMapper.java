package com.king.base.mapper.menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.menu.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 查询系统菜单列表
     * @param menu
     * @return
     */
    List<Menu> selectMenuList(Menu menu);

    /**
     * 根据用户ID查询操作权限
     * @param userId
     * @return
     */
    List<String> selectMenuPermissionByUserId(@Param("userId") String userId);


    List<Menu> selectMenuTreeAll();

    /**
     * 根据用户ID构建菜单
     * @return
     */
    List<Menu> selectMenuTreeByUserId(@Param("userId")String userId);

    /**
     * 根据角色查询菜单
     * @param rleId
     * @return
     */
    List<String> selectMenuListByRoleId(@Param("roleId")String rleId);

    /**
     * 判断是否存在子菜单
     * @param menuId
     * @return
     */
    int hasChildByMenuId(String menuId);

    /**
     * 校验菜单名称是否唯一
     * @param menu
     * @return
     */
    Menu checkMenuNameUnique(Menu menu);
}
