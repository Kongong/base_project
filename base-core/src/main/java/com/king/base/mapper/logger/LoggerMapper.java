package com.king.base.mapper.logger;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.logger.Logger;
import org.apache.ibatis.annotations.Param;

/**
 * @author Spur
 * @date 2019-11-13
 */
public interface LoggerMapper extends BaseMapper<Logger> {
    /**
     * 获取一个时间段的IP记录
     * @param date1
     * @param date2
     * @return
     */
    Long findIp(@Param("data1") String date1, @Param("data2") String date2);
}
