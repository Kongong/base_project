package com.king.base.mapper.role;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.role.RoleDept;

import java.util.List;

/**
 * @author Spur
 * @date 2019-11-12
 */
public interface RoleDeptMapper extends BaseMapper<RoleDept> {

    void deleteRoleDeptByRoleId(String roleId);

    List<String> selectDeptIdByRoleId(String roleId);
}
