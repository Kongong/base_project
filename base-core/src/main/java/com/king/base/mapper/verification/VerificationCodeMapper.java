package com.king.base.mapper.verification;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.verify.VerifyCode;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public interface VerificationCodeMapper extends BaseMapper<VerifyCode> {

    /**
     * 获取有效的验证码
     * @param scenes 业务场景，如重置密码，重置邮箱等等
     * @param type
     * @param value
     * @return
     */
    @Select("select * from verification_code where scenes=#{scenes} and type=#{type} and value=#{value} and status=1")
    VerifyCode findByScenesAndTypeAndValueAndStatusIsTrue(@Param("scenes") String scenes, @Param("type") String type, @Param("value") String value);
}
