package com.king.base.mapper.role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.domain.role.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 通过用户id查询角色
     * @param userId
     * @return
     */
    List<Role> findByUserId(@Param("userId") String userId);

    /**
     * 分页查询
     * @param page
     * @param role
     * @return
     */
    List<Role> page(Page<Role> page, @Param("bean") Role role);

    Role findByRoleCode(Role role);




}
