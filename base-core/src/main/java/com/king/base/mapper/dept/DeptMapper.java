package com.king.base.mapper.dept;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.dept.Dept;

import java.util.List;

/**
 * @author Spur
 * @date 2019/6/24
 */
public interface DeptMapper extends BaseMapper<Dept> {
    /**
     * 查询部门列表
     * @param dept
     * @return
     */
    List<Dept> selectDeptList(Dept dept);

    /**
     * 查询是否有子部门
     * @param deptId
     * @return
     */
    int hasChildByDeptId(String deptId);

    /**
     * 查询同级存在部门名称重复
     * @param dept
     * @return
     */
    int checkDeptNameUnique(Dept dept);

    /**
     * 判断部门是否存在人员
     * @param deptId
     * @return
     */
    int checkDeptExistUser(String deptId);
}
