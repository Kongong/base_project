package com.king.base.mapper.role;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.role.RoleMenu;

/**
 * @author Spur
 * @date 2019-11-5
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    /**
     * 查询菜单使用数量
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    int checkMenuExistRole(String menuId);

    /**
     * 删除角色菜单关系
     * @param roleId
     */
    void deleteRoleMenuByRoleId(String roleId);
}
