package com.king.base.mapper.visit;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.visit.Visit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public interface VisitMapper extends BaseMapper<Visit> {

    /**
     * findByDate
     * @param date
     * @return
     */
    Visit findByDate(@Param("date") String date);

    /**
     * 获得一个时间段的记录
     * @param date1
     * @param date2
     * @return
     */
    List<Visit> findAllVisits(@Param("date1") String date1, @Param("date2") String date2);
}
