package com.king.base.mapper.serial;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.serial.SerialNo;

/**
 * @author ππ
 * @date 2020-7-22 10:57
 */

public interface SerialNoMapper extends BaseMapper<SerialNo> {
    /**
     * 生成流水码
     * 示例：生成  RO202007220001
     * SerialNo serialNo = new SerialNo("role","RO",1,4);
     * serialNoMapper.generateSerialNo(serialNo);
     * System.out.println(serialNo.getNewNo());
     * @param serialNo
     * @return
     */
    SerialNo generateSerialNo(SerialNo serialNo);

}
