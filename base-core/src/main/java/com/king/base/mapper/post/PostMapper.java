package com.king.base.mapper.post;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.king.base.domain.post.Post;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Spur
 * @date 2019/6/25
 */
public interface PostMapper extends BaseMapper<Post> {

    List<Post> selectByPage(Page<Post> page, @Param("bean") Post post);

    int checkUniqueName(Post post);
}
