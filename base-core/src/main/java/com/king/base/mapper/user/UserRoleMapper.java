package com.king.base.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.king.base.domain.user.UserRole;
import org.apache.ibatis.annotations.Param;

/**
 * @author Spur
 * @date 2019-11-5
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {
    /**
     * 删除用户角色
     * @param userId
     */
    void deleteUserRole(@Param("userId") String userId, @Param("roleId")String roleId);
}
