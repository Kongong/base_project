package com.king.base.auth.security;

import java.util.Date;

/**
 * @author Spur
 * @date 2019-11-14
 */
public class OnlineUser {

    private String loginName; //登录名
    private String username; //用户名

    private String deptName; //部门

    private String browser; //浏览器

    private String ip; //ip地址

    private String address; //登录地区

    private String key; //唯一键

    private Date loginTime; //登录时间

    private String os; //操作系统

    public OnlineUser() {
    }

    public OnlineUser(String loginName,String username, String deptName, String browser, String ip, String address, String key, Date loginTime,String os) {
        this.loginName=loginName;
        this.username = username;
        this.deptName = deptName;
        this.browser = browser;
        this.ip = ip;
        this.address = address;
        this.key = key;
        this.loginTime = loginTime;
        this.os=os;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }
}
