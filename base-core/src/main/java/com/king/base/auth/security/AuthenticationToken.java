package com.king.base.auth.security;

import java.io.Serializable;

/**
 * @author TianYZ
 * @date 2019-06-15
 * 返回token
 */
public class AuthenticationToken implements Serializable {

    private  String token;

    private JwtUser user;

    public String getToken() {
        return token;
    }

    public AuthenticationToken(String token, JwtUser user) {
        this.token = token;
        this.user = user;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public JwtUser getUser() {
        return user;
    }

    public void setUser(JwtUser user) {
        this.user = user;
    }
}
