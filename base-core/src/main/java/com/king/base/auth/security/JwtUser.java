package com.king.base.auth.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.king.base.domain.role.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public class JwtUser implements UserDetails {

    private String userId;

    private String loginName;

    @JsonIgnore
    private String password;

    private String avatar;

    private String email;

    @JsonIgnore
    private Collection<? extends GrantedAuthority> authorities;

    private String status;

    private Date crtTime;

    private String username;

    private String deptId;

    private String mobilePhone;

    private String telephone;

    private String userType;

    private boolean enabled;

    private String deptName;

    private List<Role> roles;

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * 在我们保存权限的时候加上了前缀ROLE_，因此在这里需要处理下数据
     * @return
     */
//    public Collection getRoles() {
//        Set<String> roles = new LinkedHashSet<>();
//        for (GrantedAuthority authority : authorities) {
//            roles.add(authority.getAuthority().substring(5));
//        }
//        return roles;
//    }

    public String getAvatar() {
        return avatar;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public Date getCrtTime() {
        return crtTime;
    }

    public void setCrtTime(Date crtTime) {
        this.crtTime = crtTime;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public JwtUser(String userId,
                   String loginName,
                   String password,
                   String avatar,
                   String email,
                   String status,
                   Date crtTime,
                   String username,
                   String deptId,
                   String mobilePhone,
                   String telephone,
                   String userType,
                   String deptName,
                   List<Role> roles) {
        this.userId = userId;
        this.loginName = loginName;
        this.password = password;
        this.avatar = avatar;
        this.email = email;
        this.status = status;
        this.crtTime = crtTime;
        this.username = username;
        this.deptId = deptId;
        this.mobilePhone = mobilePhone;
        this.telephone = telephone;
        this.userType = userType;
        this.deptName = deptName;
        this.roles=roles;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
