package com.king.base.auth.utils;
import com.king.base.utils.ReflectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Date;

/**
 * @author Spur
 * @date 2019/6/27
 *实体类相关工具类
 *解决问题： 1、快速对实体的常驻字段，如：crtUser、crtTime、updUser等值快速注入
 */
public class EntityUtils {
    private static Logger log = LoggerFactory.getLogger(ReflectionUtils.class);
    /**
     * 快速将bean的crtUserName, crtUserId, crtTime、updUserName, updUserId, updTime附上相关值
     * @param entity 实体bean
     */
    public static <T> void setCreateAndUpdateInfo(T entity) {
        setCreateInfo(entity);
        setUpdatedInfo(entity);
    }

    /**
     * 快速将bean的crtUserName, crtUserId, crtTime附上相关值
     * @param entity 实体bean
     */
    public static <T> void setCreateInfo(T entity) {
        String userName = SecurityUtils.getName();
        String userId = SecurityUtils.getUserId();
        String deptId = SecurityUtils.getDeptId();
        // 默认属性
        String[] fieldNames = {"crtUserName", "crtUserId", "crtTime"};
        Field field = ReflectionUtils.getAccessibleField(entity, "crtTime");
        // 默认值
        Object[] value = null;
        if (field != null && field.getType().equals(Date.class)) {
            value = new Object[]{userName, userId, new Date(),deptId};
        }
        // 填充默认属性值
        setDefaultValues(entity, fieldNames, value);
    }

    /**
     * 快速将bean的updUserName, updUserId, updTime附上相关值
     * @param entity 实体bean
     */

    public static <T> void setUpdatedInfo(T entity) {
        String userName = SecurityUtils.getName();
        String userId = SecurityUtils.getUserId();
        // 默认属性
        String[] fieldNames = {"updUserName", "updUserId", "updTime"};
        Field field = ReflectionUtils.getAccessibleField(entity, "updTime");
        Object[] value = null;
        if (field != null && field.getType().equals(Date.class)) {
            value = new Object[]{userName, userId, new Date()};
        }
        // 填充默认属性值
        setDefaultValues(entity, fieldNames, value);
    }

    /**
     * 依据对象的属性数组和值数组对对象的属性进行赋值
     * @param entity 对象
     * @param fields 属性数组
     * @param value  值数组
     */
    private static <T> void setDefaultValues(T entity, String[] fields, Object[] value) {
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];
            try {
                if (ReflectionUtils.hasField(entity, field) && StringUtils.isNotEmpty(String.valueOf(value[i])) ) {
                    ReflectionUtils.invokeSetter(entity, field, value[i]);
                }
            } catch (Exception e) {
                sb.append(field).append(" ");
            }
        }
        if (!sb.toString().isEmpty()) {
            log.error(entity.getClass().getName() + ",部分字段审计失败: " + sb.toString());
        }
    }

    /**
     * 根据主键属性，判断主键是否值为空
     *
     * @param entity
     * @param field
     * @return 主键为空，则返回false；主键有值，返回true
     * @version 2016年4月28日
     */
    public static <T> boolean isPKNotNull(T entity, String field) {
        if (!ReflectionUtils.hasField(entity, field)) {
            return false;
        }
        Object value = ReflectionUtils.getFieldValue(entity, field);
        return value != null && !"".equals(value);
    }
}
