package com.king.base.auth.service;

import com.king.base.domain.role.Role;
import com.king.base.service.menu.MenuService;
import com.king.base.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 用户操作权限处理
 */
@Component
public class PermissionService
{
    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;

    /**
     * 获取角色数据权限
     * 
     * @param userId 用户信息
     * @return 角色权限信息
     */
    public Set<String> getUserRoles(String userId)
    {
        Set<String> roles = new HashSet<String>();
        // 管理员拥有所有权限
        if ("1".equals(userId)){
            roles.add("admin");
        }else{
            List<Role> rolesList=roleService.findByUserId(userId);
            if(rolesList!=null && rolesList.size()>0){
                rolesList.forEach(role -> {
                    roles.add(role.getRoleCode());
                });
            }
        }
        return roles;
    }

    /**
     * 获取菜单数据权限
     * 
     * @param userId 用户信息
     * @return 菜单权限信息
     */
    public Set<String> getUserMenuPermission(String userId)
    {
        Set<String> permissions = new HashSet<String>();
        // 管理员拥有所有权限
        if ("1".equals(userId))
        {
            permissions.add("*:*:*");
        }
        else
        {
            permissions.addAll(menuService.selectMenuPermissionByUserId(userId));
        }
        return permissions;
    }
}
