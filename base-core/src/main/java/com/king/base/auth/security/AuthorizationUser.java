package com.king.base.auth.security;

import javax.validation.constraints.NotBlank;

/**
 * @author TianYZ
 * @date 2019-06-15
 */
public class AuthorizationUser {

    private String loginName;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    private String code;

    private String uuid = "";

    /**
     * 是否需要验证码
     */
    private boolean isNeedAuthCode = true;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Override
    public String toString() {
        return "{username=" + username  + ", password= ******}";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isNeedAuthCode() {
        return isNeedAuthCode;
    }

    public void setNeedAuthCode(boolean needAuthCode) {
        isNeedAuthCode = needAuthCode;
    }
}
