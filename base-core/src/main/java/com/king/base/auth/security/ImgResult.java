package com.king.base.auth.security;

/**
 * @author TianYZ
 * @date 2019-6-5 17:29:57
 */
public class ImgResult {

    private String img;

    private String uuid;

    public ImgResult(String img, String uuid) {
        this.img = img;
        this.uuid = uuid;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
